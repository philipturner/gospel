from itertools import product
import copy
import torch
import numpy as np
from gospel.FdOperators import gradient
from gospel.Pseudopotential.KB_form import KB_form
from gospel.Pseudopotential.Pseudopotential import Pseudopotential
from gospel.util import timer, torch_to_scipy_sparse, to_cuda
from gospel.ParallelHelper import ParallelHelper

class NCPP(Pseudopotential):
    """Norm-conserving Pseudopotential. Explanation is same with Pseudopotential class.

    :type  use_dense_proj: bool
    :param use_dense_proj:
        whether use dense or sparse for KB projectors
    """

    def __init__(
        self,
        grid,
        upf,
        NLCC=True,
        use_comp=True,
        filtering=False,
        num_near_cell=1,
        kpts=[[0, 0, 0]],
        use_dense_proj=False,
    ):
        super().__init__(grid, upf, NLCC, use_comp, filtering, num_near_cell, kpts)
        assert upf.pseudo_type == "NC"
        self._has_nonlocal = False if sum(upf.num_projs.values()) == 0 else True
        self.__use_dense_proj = use_dense_proj
        if self._has_nonlocal:
            self.__KB_form = KB_form(grid, upf, num_near_cell, kpts, use_dense_proj)
            print(self.__KB_form)
        else:
            self.__KB_form = None
        return

    def __str__(self):
        s = str()
        s += "\n========================= [ Pseudopotential ] ========================="
        s += "\n* type      : NCPP"
        s += "\n* upf       : "
        for filename in self._upf.filenames:
            s += "\n    " + filename
        s += f"\n* NLCC      : {self._NLCC}"
        s += f"\n* use_comp  : {self._use_comp}"
        s += f"\n* filtering : {self._filtering}"
        s += "\n=======================================================================\n"
        return s

    @timer
    def calc_nonlocal_energy(self, occ, eigvec, use_cuda=False):
        r"""Calculate nonlocal energy from KB projectors.

        :type  occ: torch.Tensor
        :param occ:
            array of occupation numbers, shape=(nspins, nibzkpts, nbands)
        :type  eigvec: torch.Tensor
        :param eigvec:
            array of eigenvectors, shape=(nspins, nibzkpts)---(nbands, ngpts)

        :rtype: float
        :return:
            external energies from nonlocal potentials, respectively.

        ( Eq )
        E_{ext} = \sum_{\sigma} \sum_{\vec{k}}^{N_k} \sum_{i}^{occ} < \phi_{i, \vec{k}, \sigma} | \hat{V}^{NL} | \phi_{i, \vec{k}, \sigma} >
        """
        from gospel.ParallelHelper import ParallelHelper as P
        E_NL = 0.0

        if self._has_nonlocal:
            nspins, nibzkpts, nbands = occ.shape
            list_i_bands = P.split(torch.arange(nbands))
            for i_s, i_k in product(range(nspins), range(nibzkpts)):
                for idx, i_band in enumerate(list_i_bands) :
                #for i in range(nbands):
                    if abs(occ[i_s, i_k, i_band]) < 1e-7:
                        continue
                    ev = eigvec[i_s, i_k][idx]
                    val = ev.conj() @ self.get_nonlocal_op(i_k, use_cuda=use_cuda)(ev)
                    if val.dtype in [torch.complex64, torch.complex128]:
                        assert abs(val.imag) < 1e-7
                    E_NL += occ[i_s, i_k, i_band] * val.real
            E_NL = P.all_reduce(E_NL)
        return E_NL

    @timer
    def calc_forces(
        self,
        density,
        potential=None,
        deriv_density=False,
        deriv_comp=True,
    ):
        """Calculate atomic forces from nuclei. :math:`F = F_{NN} + F_{local} + F_{NL}`

        :type  density: gospel.Density
        :param density:
            It contains density, eigvec, and occ.
        :type  potential: torch.Tensor
        :param potential:
            poisson potential. (only used when use_comp==True)
        :type  deriv_density: bool
        :param deriv_density:
            derivatives of density or potential in the calculation of F_loc.
        :type  deriv_comp: bool
        :param deriv_comp:
            derivatives of compensation charge or potential in the calculation of F_comp.

        :rtype: torch.Tensor
        :return:
            atomic forces, shape=(natoms, 3)
        """
        if self._has_nonlocal:
            F_NL = self.calc_nonlocal_forces(density.eigvec, density.occ)
        else:
            F_NL = 0.0

        if self._use_comp:
            F_loc = self._compensation.calc_short_forces(
                density.get_density().sum(0),
                deriv_density=deriv_density,
            )
            F_comp = self._compensation.get_forces_correction(potential, deriv_comp)
            F = F_NL + F_loc + F_comp
            F_NN = 0.0  # debug
        else:
            F_loc = self.calc_local_forces(
                density.get_density().sum(0).numpy(),
                deriv_density=deriv_density,
            )
            F_NN = self.calc_ion_ion_forces()
            F = F_NL + F_loc + F_NN
            F_comp = 0.0  # debug

        from pprint import pprint

        pprint(f"Debug:")
        pprint(f"  F_NN   = \n{F_NN}")
        pprint(f"  F_NL   = \n{F_NL}")
        pprint(f"  F_loc  = \n{F_loc}")
        pprint(f"  F_comp = \n{F_comp}")
        pprint(f"  F      = \n{F}")
        return F

    @timer
    def calc_nonlocal_forces(self, eigvec, occ, deriv_orbital=True):
        r"""Calculate atomic forces from nonlocal potential.

        :type  eigvec: np.ndarray[torch.Tensor]
        :param eigvec:
            array of eigenvectors, shape=(nspins, nibzkpts)---(nbands, ngpts)
        :type  occ: torch.Tensor
        :param occ:
            array of occupation number, shape=(nspins, nibzkpts, nbands)
        :type  deriv_orbital: bool, optional
        :param deriv_orbital:
            derivatives of orbital or potential potential, defaults to True

        F_{NL} = -2 Re \sum_{s,k,i} f_{ski} <\psi_{ski} | V_{NL} | \nabla \psi_{ski} >
        """
        symbols = self._atoms.get_chemical_symbols()
        F = torch.zeros((len(symbols), 3), dtype=torch.float64)
        nspins, nibzkpts = eigvec.shape
        nbands = len(eigvec[0, 0])
        ngpts = self._grid.ngpts

        if deriv_orbital:
            ## Calculate derivatives of orbital
            d_eigvec = np.zeros_like(
                eigvec
            )  # shape=(nspins, nibzkpts)---(nbands, 3, ngpts)
            for i_s, i_k in product(range(nspins), range(nibzkpts)):
                d_eigvec[i_s, i_k] = torch.zeros(
                    (nbands, 3, ngpts), dtype=eigvec[i_s, i_k].dtype, device=eigvec[i_s, i_k].device
                )
                for i in range(nbands):
                    if abs(occ[i_s, i_k, i]) < 1e-7: continue
#                        d_eigvec[i_s, i_k][i] = torch.zeros(
#                            3, ngpts, dtype=torch.float64
#                        )
#                        continue
                    d_eigvec[i_s, i_k][i] = gradient(self._grid, eigvec[i_s, i_k][i])

            ## Calculate atomic forces
            for i_atom, i_sym in enumerate(symbols):
                D = self.__KB_form.get_D_matrix(i_atom)
                if len(D) == 0:  # if no projectors for this atom
                    continue
                for i_s, i_k in product(range(nspins), range(nibzkpts)):
                    P = self.__KB_form.get_KB_proj_op(i_k, i_atom)
                    #P = self.get_KB_proj(i_k, idx)
                    D = D.to(dtype=P.dtype)
                    for i in range(nbands):
                        _occ = occ[i_s, i_k, i]
                        if abs(_occ) < 1e-7:
                            continue
                        # now eigenvector and its graident are moved to cpu and computed which is super inefficient 
                        # TODO: KB proj operation on GPU
                        dpsi = d_eigvec[i_s, i_k][i].cpu()
                        _vec = eigvec[i_s, i_k][i].cpu()
                        _occ = _occ.cpu()

                        psi_P_D = (P @ _vec.T).conj().T @ D
                        for axis in range(3):
                            val = psi_P_D @ (P @ dpsi[axis])
                            F[i_atom][axis] -= 2 * _occ * val.real
        else:  # derivate nonlocal potential instead of orbital
            raise NotImplementedError
        return F

    def get_nonlocal_matrix(self, i_k):
        """Get nonlocal matrix"""
        from scipy.sparse import csr_matrix

        assert self._has_nonlocal
        P = self.__KB_form.get_KB_proj(i_k)
        D = self.__KB_form.get_D_matrix(as_sparse=True)
        return csr_matrix(P.getH() @ D @ P)

    def get_nonlocal_op(self, i_k, pytorch=True, use_cuda=False, dtype=None):
        """Return Pseudopotential's nonlocal operator

        :type  i_k: int
        :param i_k:
            index of k-point
        :type  pytorch: bool, optional
        :param pytorch:
            whether to use torch.Tensor, defaults to True
        :type  use_cuda: bool, optional
        :param use_cuda:
            whether to use CUDA to accelerate, defaults to False
        :type  dtype: torch.dtype, optional
        :param dtype:
            data type of the nonlocal operator

        :rtype: function
        :return:
            nonlocal operator function
        """
        assert self._has_nonlocal, "The nonlocal operator does not exist."
        device = ParallelHelper.get_device(use_cuda)
        if self.__KB_form.use_dense_proj:
            """
            On developing new nonlocal operator for GPU acceleration.
            (Only containing dense matrix or tensor operations, no sparse)
            """
#            P_I = self.__KB_form.KB_proj[i_k]
#            D_I = [
#                self.__KB_form.get_D_matrix(i_atom).to(P_I[0].dtype)
#                for i_atom in range(len(self._atoms))
#            ]
#            I_I = self.__KB_form.isinside
#            # if use_cuda:
#            #     for i_atom in range(len(P_I)):
#            #         P_I[i_atom] = P_I[i_atom].cuda()
#            #         D_I[i_atom] = D_I[i_atom].cuda()
#            #         I_I[i_atom] = I_I[i_atom].cuda()
#            for i_atom in range(len(P_I)):
#                #device = "cuda" if use_cuda else "cpu"
#                P_I[i_atom] = P_I[i_atom].to(device)
#                D_I[i_atom] = D_I[i_atom].to(device)
#                I_I[i_atom] = I_I[i_atom].to(device)
#
#            if dtype is not None:
#                P_I = copy.deepcopy(P_I)
#                D_I = copy.deepcopy(D_I)
#                for i_atom in range(len(P_I)):
#                    P_I[i_atom] = P_I[i_atom].to(dtype)
#                    D_I[i_atom] = D_I[i_atom].to(dtype)
#
#            def nonlocal_op(x):
#                retval = torch.zeros_like(x)  # shape=(ngpts, nbands)
#                for i_atom, (D, P, I) in enumerate(zip(D_I, P_I, I_I)):
#                    retval[I] += P.conj().T @ (D @ (P @ x[I]))
#                return retval
            def nonlocal_op(x):
                retval = torch.zeros_like(x)  # shape=(ngpts, nbands)
                P_I = self.__KB_form.KB_proj[i_k]
                D_I = [self.__KB_form.get_D_matrix(i_atom) for i_atom in range(len(self._atoms))  ]
                I_I = self.__KB_form.isinside
                for i_atom in range(len(P_I)):
                    #device = "cuda" if use_cuda else "cpu"
                    P = P_I[i_atom].to(device, dtype, True) 
                    D = D_I[i_atom].to(device, dtype, True) 
                    I = I_I[i_atom].to(device) 
                    retval[I] = retval[I] + operate_nonlocal_projector(P, D, I, x)
                    del P, D, I
                return retval
 

        else:  # sparse operator
            P = self.__KB_form.get_KB_proj(i_k)
            PH = self.__KB_form.KB_proj_H[i_k]
            D = self.__KB_form.get_D_matrix().to(P.dtype)
            if use_cuda:
                P = to_cuda(P, device)
                PH = to_cuda(PH, device)
                D = D.to( device) 
            if dtype is not None:
                _P = to_cuda(P, dtype=dtype)
                _PH = to_cuda(PH, dtype=dtype)
                _D = D.to(dtype)
                nonlocal_op = lambda x: _PH @ (_D @ (_P @ x))
            else:
                nonlocal_op = lambda x: PH @ (D @ (P @ x))

            if not pytorch:
                assert not use_cuda
                P = torch_to_scipy_sparse(P)
                D = D.numpy()
                nonlocal_op = lambda x: P.conj().T @ (D @ (P @ x))
        return nonlocal_op

    @property
    def KB_form(self):
        return self.__KB_form

    @property
    def use_dense_proj(self):
        return self.__use_dense_proj

@torch.jit.script
def operate_nonlocal_projector(P, D, I, x):
    return P.conj().T @ (D @ (P @ x[I]))
