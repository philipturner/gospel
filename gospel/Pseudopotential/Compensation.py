import copy
from itertools import product
from math import log
from time import time

import numpy as np
from ase import Atoms
from gospel.FdOperators import gradient, calc_deriv_1D
from gospel.Grid import Grid
from gospel.Pseudopotential.UPF import get_zero_convg_idx
from gospel.util import timer
from scipy.special import erf


class Compensation:
    """Compensation charge techniques. (DOI: 10.1063/1.2193514)

    :type  grid: gospel.Grid
    :param grid:
        Grid class object
    :type  upf: gospel.Pseudopotential.UPF
    :param upf:
        UPF reader
    :type  num_near_cell: int, optional
    :param num_near_cell:
        the number of near cells to consider when PBC, defaults to 1
    """

    def __init__(self, grid, upf, num_near_cell=1):
        self.__grid = grid
        self.__atoms = grid.atoms
        self.__upf = upf

        self.__r_gauss = 1.0
        self.__num_near_cell = num_near_cell
        self.__num_near = [num_near_cell if grid.get_pbc()[i] else 0 for i in range(3)]
        self.__tol = 1e-6

        self.__comp_charge = self._comp_charges_on_grid(grid, upf)
        self.__V_short, self.__short_cutoff = self._calc_V_short(
            self.__atoms, upf, self.__tol
        )

        self.__r_comp = {}
        for i_atom, i_sym in enumerate(self.__atoms.get_chemical_symbols()):
            self.__r_comp[i_sym] = self._get_r_comp(
                upf[i_sym].zval, self.__r_gauss, self.__tol
            )

        self.__energy_correction = self._calc_energy_correction(grid, upf)
        return

    def __str__(self):
        s = str()
        s += "\n=========================== [ Compensation ] =========================="
        s += f"\n symbol | r_gauss | r_short (Bohr) | r_comp (Bohr)"
        for sym in self.__upf.elements:
            s += f"\n {sym:>{6}} |"
            s += f" {np.round(self.__r_gauss, 3):>{7}} |"
            s += f" {np.round(self.__short_cutoff[sym], 3):>{14}} |"
            s += f" {np.round(self.__r_comp[sym], 3):>{13}}"
        s += "\n=======================================================================\n"
        return s

    def _calc_V_short(self, atoms, upf, tol=1e-6):
        """Calculate :math:`V^{short} = V^{loc} + V^{comp}

        V_short: short potentials (V^{loc} + V^{comp})
        short_cutoff: cutoff radii of each short potential
        """
        V_short, short_cutoff = {}, {}
        for i_atom, i_sym in enumerate(set(atoms.get_chemical_symbols())):
            V_comp = self._calc_comp_pot(upf[i_sym].zval, upf[i_sym].R)
            V_short[i_sym] = upf[i_sym].local + V_comp
            zero_convg_idx = get_zero_convg_idx(V_short[i_sym], tol)
            r_cut = upf[i_sym].R[zero_convg_idx]
            assert r_cut < 10.0, f"cutoff radius of V_short is too long. (={r_cut})"
            short_cutoff[i_sym] = r_cut
        return V_short, short_cutoff

    def _calc_comp_charge(self, Z, r):
        ## Z exp(- (r / r_gauss)^2) / (\sqrt{\pi} r_gauss)^3
        r_g = self.__r_gauss
        R = r / r_g
        return Z * np.exp(-(R**2)) / (np.sqrt(np.pi) * r_g) ** 3

    def _calc_comp_pot(self, Z, r):
        ## V = Z erf( r / r_gauss ) / r
        r_g = self.__r_gauss
        R = r / r_g
        V_c_0 = 2 * Z / r_g / np.sqrt(np.pi)
        V_c = Z * erf(R)
        V_c = np.divide(V_c, r, out=np.full(len(r), V_c_0), where=r != 0)
        return V_c

    def _calc_comp_charge_deriv(self, Z, r):
        r_g = self.__r_gauss
        R = r / r_g
        dn_c = -2 * Z * R * np.exp(-(R**2)) / np.sqrt(np.pi) ** 3 / r_g**4
        return dn_c

    def _calc_comp_pot_deriv(self, Z, r):
        r_g = self.__r_gauss
        R = r / r_g
        dV_c = Z * (2 / np.sqrt(np.pi) * R * np.exp(-(R**2)) - erf(R))
        dV_c = np.divide(dV_c, r**2, out=np.zeros_like(dV_c), where=r != 0)
        return dV_c

    def _comp_to_grid(self, sym, points, center_position, func, extplt=None):
        r_comp = self.__r_comp[sym]
        Z = self.__upf[sym].zval
        r = np.linalg.norm(points - center_position, axis=1)
        ret_val = np.zeros_like(r)
        inside = np.where(r < r_comp)[0]
        ret_val[inside] += func(Z, r[inside])
        if extplt:
            outside = np.where(r >= r_comp)[0]
            extplt = extplt.replace("r", "r[outside]")
            ret_val[outside] += eval(extplt)
        return ret_val

    def _comp_charges_on_grid(self, grid, upf):
        """Represent gaussian compensation charges of atoms on our grid."""
        atoms = grid.atoms
        atom_positions = atoms.get_positions()
        num_near = self.__num_near

        comp_charge = np.zeros(grid.ngpts)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            for i_xyz in product(
                range(-num_near[0], num_near[0] + 1),
                range(-num_near[1], num_near[1] + 1),
                range(-num_near[2], num_near[2] + 1),
            ):
                R_i = atom_positions[i_atom] + i_xyz @ atoms.get_cell()  # R_i vector
                d_r_R_i = np.linalg.norm(grid.points - R_i, axis=1)  # |r - R_i|
                comp_charge += self._calc_comp_charge(upf[i_sym].zval, d_r_R_i)
        return comp_charge

    def _get_r_comp(self, Z, r_g, tol=1e-7):
        """Calculate the radius of the compensation charge being smaller than the tolerance value."""
        val = tol * (np.sqrt(np.pi) * r_g) ** 3 / Z
        return np.sqrt(-log(val)) * r_g

    @timer
    def _calc_energy_correction(self, grid, upf):
        """Calculate energy correction by using compensation charge.

        :type  grid: gospel.Grid
        :param grid:
            Grid object
        :type  upf: gospel.Pseudopotential.UPF
        :param upf:
            UPF object
        """
        correction = 0.0
        num_near = self.__num_near
        atoms = grid.atoms

        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            Z_i = upf[i_sym].zval
            R_i = atoms.get_positions()[i_atom]  # R_i vector
            r_comp_i = self.__r_comp[i_sym]
            for j_atom, j_sym in enumerate(atoms.get_chemical_symbols()[i_atom:]):
                j_atom += i_atom
                Z_j = upf[j_sym].zval
                r_comp_j = self.__r_comp[j_sym]
                for i_xyz in product(
                    range(-num_near[0], num_near[0] + 1),
                    range(-num_near[1], num_near[1] + 1),
                    range(-num_near[2], num_near[2] + 1),
                ):
                    R_j = atoms.get_positions()[j_atom] + i_xyz @ atoms.get_cell()
                    R_ij = R_i - R_j  # (R_i - R_j) vector
                    d_ij = np.linalg.norm(R_ij)  # |R_i - R_j|

                    val = 0.0
                    if d_ij > r_comp_i + r_comp_j:
                        continue
                    else:
                        val = -self._calc_gau_gau_interaction(Z_i, Z_j, R_ij)
                    if i_atom == j_atom and i_xyz == (0, 0, 0):
                        correction += val * 0.5
                    else:
                        correction += val + Z_i * Z_j / d_ij
        return correction

    @timer
    def calc_short_forces(self, density, deriv_density=True):
        """
        Calculate atomic forces from short potential.
        (2 schemes: derivativeis of density of potential, each True of False)

        :type  density: np.ndarray
        :param density:
            density, shape=(ngpts,)
        :type  deriv_density: bool
        :param deriv_density:
            whether differentiate density or potential

        :rtype: np.ndarray
        :return:
            local part forces, shape=(natoms, 3)
        """
        atoms = self.__atoms
        upf = self.__upf

        st = time()
        ## Make small grids of each atom
        ## each grid point completely overlap with original grid's points.
        small_grids, idx_matching = self._make_small_grids(atoms, self.__short_cutoff)

        _density = density.cpu()
        if deriv_density:
            st = time()
            drho = gradient(self.__grid, density)
            print(f"deriv_density Time = {time() - st} sec")
            _drho = drho.cpu()


        F = np.zeros((len(self.__atoms), 3), dtype=float)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            Z_i = upf[i_sym].zval
            R_i = self.__atoms.get_positions()[i_atom]

            small_grid = small_grids[i_atom]
            idx_i = idx_matching[i_atom]

            if deriv_density:
                V_short = small_grid.spherical_to_grid(
                    upf[i_sym].R,
                    upf[i_sym].local,
                    R_i,
                    cutoff_radius=self.__short_cutoff[i_sym],
                )
                drho_on_small = np.zeros((3, small_grid.ngpts), dtype=float)
                for axis in range(3):
                    drho_on_small[axis] = _drho[axis].reshape(*self.__grid.gpts)[
                        idx_i[:, 0], idx_i[:, 1], idx_i[:, 2]
                    ]
                F[i_atom] = -small_grid.integrate(drho_on_small * V_short).reshape(-1)
            else:  ## calculate derivate of short potential instead of density
                r_R_i = small_grid.points - R_i  # (r - R_i) vector
                d_R_i = np.linalg.norm(r_R_i, axis=1, keepdims=True)  # |r - R_i|
                r_R_i = np.divide(
                    r_R_i, d_R_i, out=np.zeros_like(r_R_i), where=d_R_i != 0
                )  # (r - R_i)/|r - R_i|

                dV_short = small_grid.spherical_to_grid(
                    upf[i_sym].R,
                    calc_deriv_1D(upf[i_sym].R, upf[i_sym].local),
                    R_i,
                    cutoff_radius=upf[i_sym].local_cutoff,
                )
                rho_on_small = _density.reshape(*self.__grid.gpts)[
                    idx_i[:, 0], idx_i[:, 1], idx_i[:, 2]
                ].numpy()
                F[i_atom] = small_grid.integrate(
                    rho_on_small * dV_short * r_R_i.T
                ).reshape(-1)
        return F

    @timer
    def get_comp_forces(self, potential, deriv_comp=True):
        """Calculate force correction by compensation charges.

        :type  potential: np.ndarray
        :param potential:
            poisson (coulombic) potential.
        :type  deriv_comp: bool, optional
        :param deriv_comp:
            whether differentiate compensation charge or potential, defaults to True

        :rtype: np.ndarray
        :return:
            force correction, shape=(natoms, 3)
        """
        atoms = self.__atoms
        upf = self.__upf

        st = time()
        ## Make small grids of each atom
        ## each grid point completely overlap with original grid's points.
        small_grids, idx_matching = self._make_small_grids(atoms, self.__r_comp)

        if not deriv_comp:
            st = time()
            dpotential = gradient(self.__grid, potential, pytorch=False)
            print(f"Debug: dpotential Time = {time() - st} sec")

        F = np.zeros((len(atoms), 3), dtype=float)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            Z_i = upf[i_sym].zval
            R_i = atoms.get_positions()[i_atom]

            small_grid = small_grids[i_atom]
            idx_i = idx_matching[i_atom]
            r_R_i = small_grid.points - R_i  # (r - R_i) vector
            d_R_i = np.linalg.norm(r_R_i, axis=1, keepdims=True)  # |r - R_i|
            r_R_i = np.divide(
                r_R_i, d_R_i, out=np.zeros_like(r_R_i), where=d_R_i != 0
            )  # (r - R_i)/|r - R_i|

            if deriv_comp:
                V_on_small = potential.reshape(*self.__grid.gpts)[
                    idx_i[:, 0], idx_i[:, 1], idx_i[:, 2]
                ]
                dn_comp_i = self._calc_comp_charge_deriv(Z_i, d_R_i.squeeze())
                F_correct_comp_pot = -small_grid.integrate(
                    dn_comp_i * V_on_small * r_R_i.T
                ).reshape(-1)
            else:  ## derivative of potential instead of compensation charge
                n_comp_i = self._calc_comp_charge(Z_i, d_R_i.squeeze())
                dV = np.zeros((3, small_grid.ngpts), dtype=float)
                for axis in range(3):
                    dV[axis] = dpotential[axis].reshape(*self.__grid.gpts)[
                        idx_i[:, 0], idx_i[:, 1], idx_i[:, 2]
                    ]
                F_correct_comp_pot = small_grid.integrate(n_comp_i * dV).reshape(-1)
            F[i_atom] += F_correct_comp_pot
        return F

    @timer
    def get_comp_comp_forces(self):
        """Calculate (dcomp|comp) - (dloc|loc)."""
        atoms = self.__atoms
        upf = self.__upf
        num_near = self.__num_near

        F = np.zeros((len(atoms), 3), dtype=float)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            Z_i = upf[i_sym].zval
            R_i = atoms.get_positions()[i_atom]
            r_comp_i = self.__r_comp[i_sym]

            ## Calculate forces between compensation charge and compensation potential.
            for j_atom, j_sym in enumerate(atoms.get_chemical_symbols()):
                Z_j = upf[j_sym].zval
                r_comp_j = self.__r_comp[j_sym]

                for i_xyz in product(
                    range(-num_near[0], num_near[0] + 1),
                    range(-num_near[1], num_near[1] + 1),
                    range(-num_near[2], num_near[2] + 1),
                ):
                    if i_atom == j_atom:
                        continue
                    R_j = atoms.get_positions()[j_atom] + i_xyz @ atoms.get_cell()
                    R_ij = R_i - R_j
                    d_ij = np.linalg.norm(R_ij)

                    if d_ij > r_comp_i + r_comp_j:
                        continue
                    else:
                        F[i_atom] += (
                            self._calc_dgau_gau_interaction(Z_i, Z_j, R_ij)
                            + Z_i * Z_j / d_ij**3 * R_ij
                        )
        return F

    @timer
    def get_forces_correction(self, potential, deriv_comp=True):
        return self.get_comp_forces(potential, deriv_comp) + self.get_comp_comp_forces()

    def _calc_gau_gau_interaction(self, Z_i, Z_j, R_ij):
        """Calculate an interaction between two spherical gaussian charges."""
        ## using 0th-order Boys function.
        r_g_i = self.__r_gauss
        r_g_j = self.__r_gauss
        alpha = 1 / (r_g_i**2 + r_g_j**2)
        d_ij = np.linalg.norm(R_ij)
        if d_ij < 1e-7:
            tmp = 2 * np.sqrt(alpha / np.pi)
        else:
            tmp = erf(np.sqrt(alpha) * d_ij) / d_ij
        return Z_i * Z_j * tmp

    def _calc_dgau_gau_interaction(self, Z_i, Z_j, R_ij):
        """Calculate (dgau|gau)."""
        ## using 1st-order Boys function.
        r_g_i = self.__r_gauss
        r_g_j = self.__r_gauss
        alpha = 1 / (r_g_i**2 + r_g_j**2)
        d_ij = np.linalg.norm(R_ij)
        tmp = (
            np.sqrt(4 * alpha / np.pi) * np.exp(-alpha * d_ij**2) / d_ij**2
            - erf(np.sqrt(alpha) * d_ij) / d_ij**3
        )
        return Z_i * Z_j * tmp * R_ij

    def _make_small_grids_each_element(self, r_cut):
        """
        Make small grids for each atom element.
        Each grid point completely overlap with original grid's points.
        It should be modified to apply to non-cartesian grid.

        :type  r_cut: dict
        :param r_cut:
            dictionary of symbols (key) and cutoff radii (value)
        :rtype: dict
        :return:
            dictionary of symbols (key) and Grid objects (value)
        """
        grid = self.__grid
        assert (
            grid.is_cartesian
        ), "It should be modified to apply to non-cartesian grid."
        cell = grid.get_cell()
        small_grids = {}
        for sym in set(self.__atoms.get_chemical_symbols()):
            gpts = np.ceil(r_cut[sym] / grid.spacings).astype(int) * 2
            small_cell = cell * (gpts / grid.gpts).reshape(-1, 1)
            small_grid = Grid(Atoms(cell=small_cell), gpts=gpts)
            assert np.all(
                abs(small_grid.spacings - grid.spacings) < 1e-9
            ), f"{small_grid.spacings} != {self.__grid.spacings}"
            small_grids[sym] = small_grid
        return small_grids

    @timer
    def _make_small_grids(self, atoms, r_cuts):
        """Make small grids for each atom

        :type  r_cut: dict
        :param r_cut:
            dictionary of symbols (key) and cutoff radii (value)
        :rtype: tuple[list]
        :return:
            list of small_grids (list[Atoms]) and list of indices (list[np.ndarray])
        """
        small_grids = []
        small_grids_sym = self._make_small_grids_each_element(r_cuts)
        idx_matching = []
        cell = self.__grid.get_cell()
        cell_inv = np.linalg.inv(cell)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            small_grid = copy.deepcopy(small_grids_sym[i_sym])

            R_i = atoms.get_positions()[i_atom]
            nearest_idx = np.rint(R_i @ cell_inv * self.__grid.gpts)
            C_i = (nearest_idx @ cell) / self.__grid.gpts
            small_grid.translation_to(C_i)
            small_grids.append(small_grid)

            idx_shift = np.rint(
                (small_grid.points[0] - self.__grid.points[0])
                @ cell_inv
                * self.__grid.gpts
            ).astype(int)
            indices = small_grid.indices + idx_shift
            indices %= self.__grid.gpts  # PBC
            idx_matching.append(indices)
        return small_grids, idx_matching

    @property
    def comp_charge(self):
        """Compensation charge (np.ndarray), :math:`\rho^{comp}(\bm{r})`"""
        return self.__comp_charge

    @property
    def V_short(self):
        """Short potential (np.ndarray), :math:`V^{short}(\bm{r})`"""
        return self.__V_short

    @property
    def short_cutoff(self):
        return self.__short_cutoff

    @property
    def energy_correction(self):
        return self.__energy_correction

    @property
    def r_gauss(self):
        return self.__r_gauss

    @staticmethod
    def r_gauss():
        return Compensation.__r_gauss


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    """Plot n_c, V_c, dn_c, dV_c"""
    Z = 1.0
    r_g = 1.0
    r = np.arange(0, 5, 0.01)

    R = r / r_g

    n_c = Z * np.exp(-(R**2)) / (np.sqrt(np.pi) * r_g) ** 3
    V_c_0 = 2 * Z / r_g / np.sqrt(np.pi)
    V_c = Z * erf(R)
    V_c = np.divide(V_c, r, out=np.full(len(r), V_c_0), where=r != 0)

    dn_c = -2 * Z * R * np.exp(-(R**2)) / np.sqrt(np.pi) ** 3 / r_g**4
    dV_c = Z * (2 / np.sqrt(np.pi) * R * np.exp(-(R**2)) - erf(R))
    dV_c = np.divide(dV_c, r**2, out=np.zeros_like(dV_c), where=r != 0)

    tol = 1e-7
    val = tol * (np.sqrt(np.pi) * r_g) ** 3 / Z
    r_comp = np.sqrt(-log(val)) * r_g
    print("r_comp = ", r_comp)

    plt.figure()
    plt.plot(r, n_c, label="n_c")
    plt.plot(r, V_c, label="V_c")
    plt.plot(r, dn_c, label="dn_c")
    plt.plot(r, dV_c, label="dV_c")
    plt.plot(r, Z / r)
    plt.plot(r, -Z / r**2)
    plt.axhline(0)
    plt.ylim(-5, 5)
    plt.legend()
    plt.show()
