from abc import ABCMeta, abstractmethod
from itertools import product
import torch
import numpy as np
from gospel.FdOperators import gradient, calc_deriv_1D
from gospel.Pseudopotential.Compensation import Compensation
from gospel.Pseudopotential.Filtering import Filtering
from gospel.util import timer


class Pseudopotential(metaclass=ABCMeta):
    """
    This class is in charge of ion-ion and ion-electron interactions by using pseudopotential method.
    This class is parent class of several types of pseudopotential classes, e.g., NCPP and LUSP.
    Notice that only NCPP (UPF 2.1 ver) is currently supported.

    :type  grid: gospel.Grid
    :param grid:
        Grid object
    :type  upf: gospel.Pseudopotential.UPF
    :param upf:
        UPF object. It read UPF files and ready to use.
    :type  NLCC: bool
    :param NLCC:
        use non-linear core correction, defaults to True
    :type  use_comp: bool
    :param use_comp:
        whether to use compensation charge, defaults to True
    :type  filtering: bool
    :param filtering:
        use fourier filtering method
    :type  num_near_cell: int
    :param num_near_cell:
        the number of near cells to consider when PBC
    :type  kpts: list or ndarray
    :param kpts:
        list of k-points, e.g., [[0,0,0], [1,0,0], ...]

    **Example**

    >>> pp = Pseudopotential(grid, {'upf':['/upf_filename']})
    >>> E_NN = pp.ion_ion_repulsion_energy
    >>> E_ext, E_NL = pp.calc_external_energy(density, occ, eigvec, eigvec)
    """

    def __init__(
        self,
        grid,
        upf,
        NLCC=True,
        use_comp=True,
        filtering=False,
        num_near_cell=1,
        kpts=[[0, 0, 0]],
    ):
        self._grid = grid
        self._atoms = grid.atoms
        self._upf = upf
        self._NLCC = NLCC
        self._use_comp = use_comp
        self._filtering = filtering
        self._num_near_cell = num_near_cell
        self._kpts = kpts
        self._has_nonlocal = False

        ## When use_comp=True, compensate local potential
        if use_comp:
            self._compensation = Compensation(grid, upf, self._num_near_cell)
            print(self._compensation)
            upf.local = self._compensation.V_short
            upf.local_cutoff = self._compensation.short_cutoff
            self._ion_ion_repulsion_energy = 0.0
        else:
            self._ion_ion_repulsion_energy = self.calc_ion_ion_repulsion()

        ## When filtering=True, filtering local and KB projectors
        if filtering:
            assert use_comp, "'filtering' is only supported with 'use_comp'=True"
            self._filter = Filtering(max(grid.spacings))
            upf.local, upf.local_cutoff = self._filter.filtering(upf, "local")
            upf.beta, upf.beta_cutoff = self._filter.filtering(upf, "nonlocal")

        ## Calculate the number of valence electrons
        self._num_valence_e = 0
        for i_atom, i_sym in enumerate(self._atoms.get_chemical_symbols()):
            self._num_valence_e += upf[i_sym].zval

        self._V_ext = self.calc_local_pot(grid, upf, use_comp)
        return

    @timer
    def calc_local_pot(self, grid, upf, use_comp, fine=None):
        """Read local potential from upf and represent it on grid.

        :type  grid: gospel.Grid
        :param grid:
            Grid class object
        :type  upf: gospel.Pseudopotential.UPF
        :param upf:
            UPF class object
        :type  use_comp: bool
        :param use_comp:
            whether to use compensation charge

        :rtype: torch.Tensor
        :return:
            local potential, shape=(ngpts,)

        Eq) V^{\text{loc}}
            = \sum_{i} V^{\text{loc}}_{i}
            = \sum_{i} \int \frac{n^{\text{loc}}_{i}}{|\vec{r} - \vec{r}'} d\vec{r}'

            When compensation charge is used (DOI: 10.1063/1.2193514),
            V^{\text{loc}} + V^{\text{comp}}
            = \sum_{i} ( V^{\text{loc}}_{i} + V^{\text{comp}}_{i} )
            = V^{\text{short}}
            : short-ranged potential
        """
        atoms = grid.atoms  # ase.Atoms object

        if isinstance(fine, int):
            from gospel.Grid import Grid

            original_grid = grid
            grid = Grid(atoms, grid.gpts * fine)

        local_pot = torch.zeros(grid.ngpts, dtype=torch.float64)
        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            R_i = atoms.get_positions()[i_atom]
            extplt = None if use_comp else f"-{upf[i_sym].zval}/r"
            local_pot += grid.spherical_to_grid(
                upf[i_sym].R,
                upf[i_sym].local,
                R_i,
                extplt=extplt,
                cutoff_radius=upf[i_sym].local_cutoff,
                num_near_cell=self._num_near_cell,
            )

        if isinstance(fine, int):
            from Supersampling import TruncSincSupersampling3D, run

            conv = TruncSincSupersampling3D(
                [fine, fine, fine], [5, 5, 5], original_grid.spacings
            )
            local_pot = run(conv, local_pot.reshape(grid.gpts), grid.get_pbc())
            local_pot = local_pot.reshape(-1)
            grid = original_grid
            assert len(local_pot) == grid.ngpts
        return local_pot

    def get_nuclear_pot(self):
        """- Z/r potential"""
        nuclear_pot = np.zeros(self._grid.ngpts)
        for i_atom, i_sym in enumerate(self._atoms.get_chemical_symbols()):
            d_r_R_i = np.linalg.norm(
                self._grid.points - self._atoms.get_position()[i_atom], axis=1
            )  # |r - R_i|
            nuclear_pot -= self._atoms.get_atomic_numbers() / d_r_R_i
        return nuclear_pot

    def init_density(self):
        """Guess initial density from PP_RHOATOMs in pseudopotential files."""
        density = np.zeros(self._grid.ngpts)
        atom_positions = self._atoms.get_positions()

        for i_atom, i_sym in enumerate(self._atoms.get_chemical_symbols()):
            density += self._grid.spherical_to_grid(
                self._upf[i_sym].R,
                self._upf[i_sym].rhoatom,
                atom_positions[i_atom],
                cutoff_radius=self._upf[i_sym].rho_cutoff,
                num_near_cell=self._num_near_cell,
            )
        return density

    def calc_ion_ion_repulsion(self):
        """Calculate ion-ion repulsion energy.
        Eq) E_{ion-ion} = \frac{1}{2} \sum_{i > j} \frac{ Z_{i} Z_{j} }{ r_{ij} }
        """
        ion_ion = 0.0
        atoms = self._atoms
        atom_positions = atoms.get_positions()
        num_near = [self._num_near_cell if atoms.get_pbc()[i] else 0 for i in range(3)]

        for i_atom, i_sym in enumerate(atoms.get_chemical_symbols()):
            Z_i = self._upf[i_sym].zval
            R_i = atom_positions[i_atom]
            for j_atom, j_sym in enumerate(atoms.get_chemical_symbols()[i_atom:]):
                j_atom += i_atom
                Z_j = self._upf[j_sym].zval
                for i_xyz in product(
                    range(-num_near[0], num_near[0] + 1),
                    range(-num_near[1], num_near[1] + 1),
                    range(-num_near[2], num_near[2] + 1),
                ):
                    if i_atom == j_atom and i_xyz == (0, 0, 0):
                        continue
                    R_j = atom_positions[j_atom] + i_xyz @ atoms.get_cell()
                    R_ij = R_i - R_j
                    d_ij = np.linalg.norm(R_ij)
                    ion_ion += Z_i * Z_j / d_ij
        return ion_ion

    def calc_ion_ion_forces(self):
        atoms = self._atoms
        symbols = atoms.get_chemical_symbols()
        upf = self._upf
        atom_positions = atoms.get_positions()
        num_near = [self._num_near_cell if atoms.get_pbc()[i] else 0 for i in range(3)]
        F = np.zeros((len(symbols), 3), dtype=float)

        for i_atom, i_sym in enumerate(symbols):
            Z_i = upf[i_sym].zval
            R_i = atom_positions[i_atom]
            for j_atom, j_sym in enumerate(symbols):
                Z_j = upf[j_sym].zval
                for i_xyz in product(
                    range(-num_near[0], num_near[0] + 1),
                    range(-num_near[1], num_near[1] + 1),
                    range(-num_near[2], num_near[2] + 1),
                ):
                    if i_atom == j_atom:
                        continue
                    R_j = atom_positions[j_atom] + i_xyz @ atoms.get_cell()
                    R_ij = R_i - R_j
                    d_ij = np.linalg.norm(R_ij)
                    F[i_atom] += (Z_i * Z_j / d_ij**3) * R_ij
        return F

    @timer
    def calc_NLCC_core_density(self):
        """Represent NLCC core densites on grid from spherical coordinates."""
        atom_positions = self._atoms.get_positions()
        NLCC_core_density = np.zeros(self._grid.ngpts)
        for i_atom, i_sym in enumerate(self._atoms.get_chemical_symbols()):
            if self._upf[i_sym].nlcc is None:
                continue
            else:
                NLCC_core_density += self._grid.spherical_to_grid(
                    self._upf[i_sym].R,
                    self._upf[i_sym].nlcc,
                    atom_positions[i_atom],
                    cutoff_radius=self._upf[i_sym].nlcc_cutoff,
                    num_near_cell=self._num_near_cell,
                )
        return NLCC_core_density

    def calc_local_forces(self, density, deriv_density=True):
        r"""Calculate atomic forces from local potential. (2 schemes: derivativeis of density of potential, each True of False)

        :type  density: np.ndarray
        :param density:
            density values, shape=(ngpts,)
        :type  deriv_density: bool
        :param deriv_density:
            derivatives of density or potential, defaults to True

        :rtype: np.ndarray
        :return:
            local part forces, shape=(natoms, 3)

        if deriv_density is False
        F^{a}_{loc} = \int \frac{\round V^{a}_{loc}(r)}{\round r} \rho(r)
                      (\vec{r} - \vec{R}_{a}) / |\vec{r} - \vec{R}_{a}| d\vec{r}

        if deriv_density is True
        F^{a}_{loc} = - \int V^{a}_{loc}(r) \nabla \rho(r) d\vec{r}
        """
        symbols = self._atoms.get_chemical_symbols()
        upf = self._upf
        F = np.zeros((len(symbols), 3), dtype=float)

        if not deriv_density:  # derivate potential
            for i_atom, i_sym in enumerate(symbols):
                R_i = self._atoms.get_positions()[i_atom]
                local_deriv = calc_deriv_1D(upf[i_sym].R, upf[i_sym].local)
                dV_loc = self._grid.spherical_to_grid(
                    upf[i_sym].R,
                    calc_deriv_1D(upf[i_sym].R, upf[i_sym].local),
                    R_i,
                    extplt=f"{upf[i_sym].zval}/r**2",
                    cutoff_radius=upf[i_sym].local_cutoff,
                    num_near_cell=self._num_near_cell,
                )
                r_R_i = self._grid.points - R_i  # r_R_i.shape = (N_g, 3)
                d_r_R_i = np.linalg.norm(
                    r_R_i, axis=1, keepdims=True
                )  # d_r_R_i.shape = (N_g, 1)
                r_R_i = np.divide(
                    r_R_i, d_r_R_i, out=np.zeros_like(r_R_i), where=d_r_R_i != 0
                )  # (r - R_i)/|r - R_i|
                F[i_atom] = self._grid.integrate(density * dV_loc * r_R_i.T).reshape(-1)
        else:  # derivate density instead of potential
            drho = gradient(self._grid, density, pytorch=False)
            for i_atom, i_sym in enumerate(symbols):
                R_i = self._atoms.get_positions()[i_atom]
                V_loc = self._grid.spherical_to_grid(
                    upf[i_sym].R,
                    upf[i_sym].local,
                    R_i,
                    extplt=f"-{upf[i_sym].zval}/r",
                    cutoff_radius=upf[i_sym].local_cutoff,
                    num_near_cell=self._num_near_cell,
                )
                F[i_atom] = -self._grid.integrate(V_loc * drho).reshape(-1)
        return F

    @timer
    def calc_external_energy(self, density):
        """Calculate external energy

        :type  density: gospel.Density
        :param density:
            Density class object

        :rtype: float
        :return:
            external energy

        ( Eq )
        E_{ext} = \int \rho(\vec{r}) V^{loc}(\vec{r}) d\vec{r}
        """
        E_ext = self._grid.integrate(density.get_density().sum(0).cpu() * self._V_ext)
        if self._use_comp:
            E_ext += self._compensation.energy_correction
        return E_ext

    def get_comp_charge(self):
        if self._use_comp:
            return self._compensation.comp_charge
        else:
            return 0

    def get_S(self):
        return None

    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    def calc_forces(self):
        pass

    @property
    def num_valence_e(self):
        return self._num_valence_e

    @property
    def use_comp(self):
        return self._use_comp

    @property
    def NLCC(self):
        return self._NLCC

    @property
    def V_ext(self):
        return self._V_ext

    @property
    def ion_ion_repulsion_energy(self):
        return self._ion_ion_repulsion_energy

    @property
    def has_nonlocal(self):
        return self._has_nonlocal
