from time import time
import numpy as np
from scipy.special import sph_harm
import scipy.sparse as sparse
import torch


def timer(func):
    """Check time.

    Usage)
    >>> @timer
    >>> def method1(...):
    >>>     ...
    >>>     return
    """

    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        print("Elapsed time[{}]: {} sec".format(func.__name__, (end - start)), flush=True)
        return result

    return wrapper

#Custom Decorator function
# from  https://stackoverflow.com/questions/49210801/python3-pass-lists-to-function-with-functools-lru-cache
def toTuple(function):
    def wrapper(*args, **kwargs):
        args = [tuple(x.reshape(-1)) if type(x) == np.ndarray else x for x in args]
        args = [tuple(x) if type(x) == list else x for x in args]
        for key, values in kwargs.items():
            if type(values)==np.ndarray:
                kwargs[key ] = tuple(values.reshape(-1) ) 
            elif type(values)==list:
                kwargs[key ] = tuple(values ) 

        result = function(*args, **kwargs)
        #result = tuple(result) if type(result) == list else result
        return result
    return wrapper

def Y_lm(point, l, m, tol=1e-7):
    """
    Calculate Real Spherical harmonics.

    :type  point: np.ndarray
    :param point:
        grid points, shape=(ngpts, 3)
    :type  l: int
    :param l:
        angular momentum quantum number
    :type  m: int
    :param m:
        magnetic momentum quantum number, |m| <= l

    :rtype: np.ndarray
    :return:
        real spherical harmonics values, shape=(ngpts,)

    phi   : Polar coordinate [0,pi]
    theta : Azimuthal coordinate [0,2*pi]

            z
            |
            |
            |_______ y
            /
           /
          x

    phi   = arctan( \frac{\sqrt{x^2+y^2}}{z} )
    theta = arccos( \frac{x}{\sqrt{x^2+y^2}} )
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        # ex)
        # >>> np.nan_to_num( [nan, -inf, inf, nan] )
        # >>> array([0.00000000e+000, -1.79769313e+308, 1.79769313e+308, 0.00000000e+000])
        rxy = np.sqrt(point[:, 0] ** 2 + point[:, 1] ** 2)
        phi = np.arctan(
            np.where(
                (point[:, 2] == 0) * (rxy == 0) == True,
                1.0,
                np.nan_to_num(rxy / point[:, 2]),
            )
        )
        phi = np.where(point[:, 2] < 0, phi + np.pi, phi)
        theta = np.arccos(
            np.where(
                (point[:, 0] == 0) * (rxy == 0) == True,
                1.0,
                np.nan_to_num(point[:, 0] / rxy),
            )
        )
        theta = np.where((point[:, 1] < 0) == True, -theta + 2 * np.pi, theta)

    ## Make Real Spherical harmonics
    if m > 0:
        real_sph_harm = (-1) ** m * np.sqrt(2) * sph_harm(m, l, theta, phi).real
        return real_sph_harm
    elif m < 0:
        real_sph_harm = (-1) ** m * np.sqrt(2) * sph_harm(abs(m), l, theta, phi).imag
        return real_sph_harm
    else:  # m == 0
        return sph_harm(m, l, theta, phi).real


def tensordot(a, b, axes=2):
    """multiplication of sparse matrix and dense tensor. (modification of np.tensordot)

    :type  a: np.ndarray or scipy.sparse.spmatrix
    :type  b: np.ndarray or scipy.sparse.spmatrix
    :type  axes: int or tuple

    :rtype: np.ndarray
    """
    if isinstance(a, sparse.spmatrix) or isinstance(a, np.ndarray):
        assert isinstance(b, sparse.spmatrix) or isinstance(b, np.ndarray)
        _transpose = np.transpose
    elif isinstance(a, torch.Tensor):
        assert isinstance(b, torch.Tensor)
        if not (a.layout == b.layout == torch.strided):
            raise NotImplementedError("torch sparse tensor is not supported.")
        _transpose = torch.permute

    try:
        iter(axes)
    except Exception:
        axes_a = list(range(-axes, 0))
        axes_b = list(range(0, axes))
    else:
        axes_a, axes_b = axes
    try:
        na = len(axes_a)
        axes_a = list(axes_a)
    except TypeError:
        axes_a = [axes_a]
        na = 1
    try:
        nb = len(axes_b)
        axes_b = list(axes_b)
    except TypeError:
        axes_b = [axes_b]
        nb = 1

    # a, b = asarray(a), asarray(b)
    as_ = a.shape
    nda = a.ndim
    bs = b.shape
    ndb = b.ndim
    equal = True
    if na != nb:
        equal = False
    else:
        for k in range(na):
            if as_[axes_a[k]] != bs[axes_b[k]]:
                equal = False
                break
            if axes_a[k] < 0:
                axes_a[k] += nda
            if axes_b[k] < 0:
                axes_b[k] += ndb
    if not equal:
        raise ValueError("shape-mismatch for sum")

    # Move the axes to sum over to the end of "a"
    # and to the front of "b"
    notin = [k for k in range(nda) if k not in axes_a]
    newaxes_a = notin + axes_a
    N2 = 1
    for axis in axes_a:
        N2 *= as_[axis]
    newshape_a = (int(np.multiply.reduce([as_[ax] for ax in notin])), N2)
    olda = [as_[axis] for axis in notin]

    notin = [k for k in range(ndb) if k not in axes_b]
    newaxes_b = axes_b + notin
    N2 = 1
    for axis in axes_b:
        N2 *= bs[axis]
    newshape_b = (N2, int(np.multiply.reduce([bs[ax] for ax in notin])))
    oldb = [bs[axis] for axis in notin]

    if isinstance(a, sparse.spmatrix):
        if np.all(newaxes_a == [1, 0]):
            at = a.T.reshape(newshape_a)
        elif np.all(newaxes_a == [0, 1]):
            at = a.reshape(newshape_a)
    else:
        # at = a.transpose(newaxes_a).reshape(newshape_a)
        at = _transpose(a, newaxes_a).reshape(newshape_a)
    if isinstance(b, sparse.spmatrix):
        if np.all(newaxes_b == [1, 0]):
            bt = b.T.reshape(newshape_b)
        elif np.all(newaxes_b == [0, 1]):
            bt = b.reshape(newshape_b)
    else:
        # bt = b.transpose(newaxes_b).reshape(newshape_b)
        bt = _transpose(b, newaxes_b).reshape(newshape_b)

    res = at @ bt
    return res.reshape(olda + oldb)


def torch_to_scipy_sparse(A_torch):
    """convert torch.sparse to scipy.sparse

    :type  A_torch: torch.Tensor
    :param A_torch:
        torch sparse layout matrix (sparse_csr or sparse_coo)

    :rtype: scipy.sparse._csr.csr_matrix or scipy.sparse._coo.coo_matrix
    :return:
        scipy sparse type matrix
    """
    assert isinstance(A_torch, torch.Tensor)
    if A_torch.layout == torch.sparse_csr:
        A_scipy = sparse.csr_matrix(
            (
                A_torch.values().numpy(),
                A_torch.col_indices().numpy(),
                A_torch.crow_indices().numpy(),
            ),
            shape=np.asarray(A_torch.size()),
        )
    elif A_torch.layout == torch.sparse_coo:
        row, col = A_torch.indices()
        A_scipy = sparse.coo_matrix(
            (A_torch.values().numpy(), (row, col)),
            shape=np.asarray(A_torch.size()),
        )
        pass
    else:
        raise NotImplementedError
    return A_scipy


def scipy_to_torch_sparse(A_scipy):
    """convert scipy.sparse to torch.sparse

    :type  A_scipy: scipy.sparse._csr.csr_matrix or scipy.sparse._coo.coo_matrix
    :param A_scipy:
        scipy sparse type matrix

    :rtype: torch.Tensor
    :return:
        torch sparse type tensor
    """
    if isinstance(A_scipy, sparse.csr_matrix):
        A_torch = torch.sparse_csr_tensor(
            A_scipy.indptr,
            A_scipy.indices,
            A_scipy.data,
            A_scipy.shape,
        )
    elif isinstance(A_scipy, sparse.coo_matrix):
        A_torch = torch.sparse_coo_tensor(
            np.vstack((A_scipy.row, A_scipy.col)),
            A_scipy.data,
            A_scipy.shape,
        )
    else:
        raise NotImplementedError(f"type(A_scipy) = {type(A_scipy)}")
    return A_torch


def torch_diag_sparse(inp, dtype=None):
    """convert dense Tensor to diagonal CSR sparse tensor

    :type  inp: torch.Tensor
    :param inp:
        diagonal elements, shape=(N,)
    :type  dtype: torch.dtype, optional
    :param dtype:
        data type

    :rtype: torch.Tensor (layout=torch.sparse_csr)
    :return:
        diagonal CSR sparse tensor, shape=(N, N)
    """
    if isinstance(input, torch.Tensor):
        raise TypeError(f"expected torch.Tensor (got {type(input)})")
    indices = torch.arange(len(inp), device=inp.device)
    indices = torch.vstack((indices, indices))
    output = torch.sparse_coo_tensor(indices, inp, (len(inp), len(inp)), dtype=dtype)
    return output.to_sparse_csr()


def to_cuda(inp, device=None, dtype=None):
    """Returns a copy object of torch.sparse_csr in device

    :type  inp: torch.Tensor
    :param inp:
        torch sparse csr tensor
    """
    assert isinstance(inp, torch.Tensor)
    if device is None:
        device = inp.device
    else:
        assert isinstance(device, torch.device)

    #assert inp.layout == torch.sparse_csr, f"{inp.layout}, {type(inp)}"
    if inp.layout == torch.sparse_csr:
        output = torch.sparse_csr_tensor(
            inp.crow_indices().to(device),
            inp.col_indices().to(device),
            inp.values().to(device),
            inp.size(),
            dtype=dtype, device=device
        )
    elif inp.layout == torch.sparse_csc:
        output = torch.sparse_csc_tensor(
            inp.ccol_indices().to(device),
            inp.row_indices().to(device),
            inp.values().to(device),
            inp.size(),
            dtype=dtype, device=device
        )
    else:
        raise NotImplementedError  
    return output
