from itertools import product
import numpy as np
import torch
from math import sqrt
from gospel.LinearOperator import LinearOperator, aslinearoperator
from gospel.Eigensolver.Eigensolver import Eigensolver, gen_eigh
from gospel.util import timer
import time
from gospel.ParallelHelper import ParallelHelper as P

torch.backends.cuda.matmul.allow_tf32 = False
print(
    f"torch.backends.cuda.matmul.allow_tf32 : {torch.backends.cuda.matmul.allow_tf32}"
)
torch.manual_seed(0)


def vprint(*args, verbosity=True):
    if verbosity:
        for arg in args:
            print(arg, end="")
        print()
    else:
        pass


class Davidson(Eigensolver):
    """Block Davidson method. (https://arxiv.org/abs/0906.2569)

    :type maxiter: int, optional
    :param maxiter:
        the maximum number of iterations, defaults to 20
    :type nblock: int, optional
    :param nblock:
        the maximum size of block, defaults to 3
    :type convg_tol: float or None, optional
    :param convg_tol:
        tolerance of convergence, defaults to None.
        ('convg_tol' is set to '(dimension) * 10**(-15/2)' as a default value.)
    :type locking: bool
    :param locking:
        whether locking converged eigenvectors or not
    :type verbosity: int, optional
    :param verbosity:
        verbose level (0 or 1), defaults 1
    :type fill_block: bool, optional
    :param fill_block:
        defaults to True
    """

    def __init__(
        self,
        maxiter=5,
        nblock=2,
        convg_tol=None,
        locking=True,
        verbosity=0,
        fill_block=True,
        dynamic=False,
        dynamic_tol=None,
        dynamic_maxiter=None,
    ):
        super().__init__()
        self._type = "davidson"
        self.__maxiter = maxiter
        self.__convg_tol = convg_tol
        self.__nblock = nblock
        self.__locking = locking
        self.__verbosity = verbosity
        self.__fill_block = fill_block

        self.__dynamic = dynamic
        self.__dynamic_tol = dynamic_tol
        self.__dynamic_maxiter = dynamic_maxiter
        return

    def __str__(self):
        s = str()
        s += f"\n===================== [ Eigensolver(Davidson) ] ===================="
        s += f"\n* convg_tol       : {self.__convg_tol}"
        s += f"\n* maxiter         : {self.__maxiter}"
        s += f"\n* nblock          : {self.__nblock}"
        s += f"\n* locking         : {self.__locking}"
        s += f"\n* verbosity       : {self.__verbosity}"
        s += f"\n* fill_block      : {self.__fill_block}"
        s += f"\n* dynamic         : {self.__dynamic}"
        s += f"\n* dynamic_tol     : {self.__dynamic_tol}"
        s += f"\n* dynamic_maxiter : {self.__dynamic_maxiter}"
        s += f"\n====================================================================\n"
        return str(s)

    @timer
    def diagonalize(self, hamiltonian, i_scf_iter=None):
        """Diagonalize hamiltonians corresponding each spin and k-points.

        :type  hamiltonian: gospel.Hamiltonian
        :param hamiltonian:
            Hamiltonian class object
        :type  i_scf_iter: int or None, optional
        :param i_scf_iter:
            index of SCF iteration

        :rtype: (np.ndarray, np.ndarray)
        :return:
            eigenvalues and eigenvectors,
            eigval.shape=(nspins, nibzkpts, nbands)
            eigvec.shape=(nspins, nibzkpts)---(nbands, ngpts)
        """
        # orthogonalization will be performed in davidson function thus it is skipped in initialization
        self._initialize_guess(hamiltonian, False) 

        ## Solve eigenvalue problem
        from gospel.ParallelHelper import ParallelHelper as P
        eigval = torch.zeros_like(self._starting_value)
        eigvec = np.zeros_like(self._starting_vector)  # eigvec.dtype = object
        for i_s, i_k in product(range(eigvec.shape[0]), range(eigvec.shape[1])):
            A = hamiltonian[i_s, i_k]
            solve_options = {
                "A": A,
                "X": self._starting_vector[i_s, i_k].to(eigval.device),
                "B": hamiltonian.get_S(device=eigval.device),
                "preconditioner": self.preconditioner,
                "tol": self.__convg_tol,
                "maxiter": self.__maxiter if i_scf_iter!=0 else self.__maxiter+5,
                "nblock": self.__nblock,
                "locking": self.__locking,
                "fill_block": self.__fill_block,
                # "dynamic": self.__dynamic,
                # "dynamicTol": self.__dynamic_tol,
                # "dynamicMaxiter": self.__dynamic_maxiter,
                "verbosityLevel": self.__verbosity,
                #"ortho":'qr',
                "i_scf_iter": i_scf_iter, 
            }
            val, vec = davidson(**solve_options)
            #val, vec = val.cpu(), vec.cpu()
            #print(f'test2 {P.rank}')
            eigval[i_s, i_k], eigvec[i_s, i_k] = val, vec.T
            #eigvec[i_s, i_k] = vec.T  ## memory copy.. (not eifficient code)
            #print(f'test4 {P.rank}')

            ## update starting vectors and values
            self._starting_value[i_s, i_k] = val #eigval[i_s, i_k]
            self._starting_vector[i_s, i_k] = P.merge(vec)
        return eigval, eigvec

    @property
    def convg_tol(self):
        return self.__convg_tol

    @convg_tol.setter
    def convg_tol(self, inp):
        self.__convg_tol = inp
        return

    @property
    def dynamic(self):
        return self.__dynamic

def davidson(
    A,
    X,
    B=None,
    preconditioner=None,
    tol=1e-4,
    maxiter=20,
    nblock=2,
    locking=True,
    fill_block=True,
    verbosityLevel=0,
    ortho="cholesky",
    retHistory=False,
    i_scf_iter = 0,
):
    """Block Davidson eigenvalue problem solver.

    :type A: LinearOperator
    :param A:
        Hamiltonian operator
    :type X: torch.Tensor
    :param X:
        guess eigenvectors
    :type B: LinearOperator or None, optional
    :param B:
        overlap matrix for generalized eigenvalue problem
    :type M: LinearOperator or None, optional
    :param M:
        precondition operator
    :type tol: float, optional
    :param tol:
        convergence tolerance, defaults to 1e-4.
    :type maxiter: int, optional
    :param maxiter:
        the maximum number of iterations, defaults to 20
    :type nblock: int, optional
    :param nblock:
        the maximum size of block, defaults to 2
    :type locking: bool, optional
    :param locking:
        whether locking converged eigenvectors or not, defaults to True
    :type dynamic: bool, optional
    :param dynamic:
        whether using dynamic precision method or not, defaults to False
    :type dynamicTol: float, optional
    :param dynamicTol:
        convergence tolerance for dynamic precision
        ('dynamicTol' is set to '(dimension) * 10**(-15/4)' as a default value.)
    :type dynamicMaxiter: int, optional
    :param dynamicMaxiter:
        the maximum number of iterations for dynamic precision, defaults to 10
    :type verbosityLevel: int, optional
    :param verbosityLevel:
        verbosity level, defaults to 0
    :type ortho: str, optional
    :param ortho:
        orthogonalize method, options=["qr", "cholesky"], defaults to "cholesky"

    :rtype: tuple[torch.Tensor]
    :return:
        eigenvalues and eigenvectors
    """
    # all  : residue, A_sub_list, B_sub_list, eigval, C, is_convg,
    # split: AX, BX, R, is_convg, U, X, my_unlock, my_convg
    neig = X.shape[-1]
    device = X.device
    fp_type = X.dtype
    A = aslinearoperator(A)
    B = aslinearoperator(B)
    #M = aslinearoperator(M)
    ## initializatioin
    st = time.time()
    if(P.is_master()):
        try:
            initial_X = _orthonormalize(X, method=ortho).contiguous()
        except torch.cuda.OutOfMemoryError:
            print('WARNING: Orthonormalization is performed on CPU')
            initial_X = _orthonormalize(X.cpu(), method=ortho).contiguous().to(device)
    else:
        initial_X = torch.empty_like(X)

    assert initial_X is not None, f"orthogonalization failed!"

#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(-5): {et-st} s, {type(initial_X)}')
#    st = time.time()
    initial_X = P.broadcast(initial_X)
    X = P.split(initial_X).clone()
#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(-4): {et-st} s')
#    st = time.time()
    AX = A @ X
#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(-3): {et-st} s')
#    st = time.time()
    A_sub, list_size = P.merge(initial_X.T.conj()@ AX, return_list_size=True )
#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(-2): {et-st} s')
#    st = time.time()

    if B is None:
        del initial_X;
        BX = X  # shared data
        eigval, C = torch.linalg.eigh(A_sub)
    else:
        BX = B @ X
        B_sub = P.merge(initial_X.T.conj()@ BX, list_size=list_size)
        del initial_X;
        eigval, C = gen_eigh(A_sub, B_sub)

#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(-1): {et-st} s')
#    st = time.time()
    eigval, C = eigval[:neig], C[:, :neig]
#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(0-1): {et-st} s')
#    st = time.time()

    vprint("-*" * 30, "i_iter=1", verbosity=verbosityLevel)
    vprint(f"eigenvalue: {eigval}", verbosity=verbosityLevel)
    X  = P.reduce_scatter( X @ P.split(C, dim=0),dim=1 )
    AX = P.reduce_scatter(AX @ P.split(C, dim=0),dim=1 )
    BX = P.reduce_scatter(BX @ P.split(C, dim=0),dim=1 ) if B is not None else X
#    torch.cuda.synchronize(device)
#    et = time.time()
#    print(f'(0-2): {et-st} s')
    
    # Save convergence history
    eigHistory = []
    resHistory = []


    ## Start iteration
    find = False
    sub_dim = 0

    unlock = torch.full((neig,), True, device=device)
    my_unlock  = P.split(unlock.clone())
    preconditioner.reset_num_called()



    for i_iter in range(2, maxiter + 1):
        vprint("\n" + "=*" * 50, f"i_iter={i_iter}", verbosity=verbosityLevel)
        list_AU_idx = [0]

        U = X
        AU_list = [AX]
 
#        st = time.time()
        merge_AX, list_size = P.merge( AX, return_list_size=True)
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(f'{i_iter} (1-1): {et-st} s', flush=True)
#        st = time.time()
        A_sub_list = [P.merge(X.conj().T@merge_AX, 0, list_size=list_size) ]
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(f'{i_iter} (1-2): {et-st} s', flush=True)
#        st = time.time()
        if B is not None:
            BU_list = [BX]
            B_sub_list = P.merge(X.conj().T@P.merge( BX, list_size=list_size) ,0, list_size=list_size)
        del merge_AX
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(f'{i_iter} (1-3): {et-st} s', flush=True)
#        st = time.time()

        ## Start block expansion
        sub_dim = A_sub_list[-1].shape[-1]  # dimension of the subspace
        sub_dim_list = [ sub_dim ]
        i_b = 1  # the number of expansions

        ## current rank's index for U and C
        global_index    =  P.split(torch.arange(C.size(-1), dtype=torch.int64, device=device ))

#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(f'{i_iter} (1-4): {et-st} s', flush=True)
        while True:
            st = time.time()
            ## Check the number of expansions
            if fill_block:
                if sub_dim == nblock * neig:
                    vprint(f"Stop block expansion.", verbosity=verbosityLevel)
                    break
            else: 
            #if not fill_block:
                if i_b == nblock:
                    vprint(
                        f"Stop block expansion. ({i_b} by {i_b})",
                        verbosity=verbosityLevel,
                    )
                    break

            count_unlock = [torch.sum(_) for _ in P.split(unlock, return_all=True)] if P.global_size>1 else [ torch.sum(unlock) ]

            ## Calculate residual
            R = AX[:, my_unlock ] - P.split(eigval)[my_unlock] * BX[:, my_unlock]
            my_residue = torch.linalg.norm(R, dim=0)
            residue = P.merge(my_residue, list_size=count_unlock)


            vprint(f"residual norms: {residue}", verbosity=verbosityLevel)
            if retHistory and i_b == 1:
                eigHistory.append(eigval.to("cpu"))
                resHistory.append(residue.to("cpu"))

            ## Check convergence and lock converged states
            is_convg = residue < tol
            my_convg = my_residue <tol
            if locking:
                my_unlock[my_unlock.clone()] =  ~my_convg  #update my_unlock
                unlock [unlock.clone()] = ~is_convg
                R = R[:,  ~my_convg  ] # update R
                #count_unlock = [torch.sum(_) for _ in P.split(unlock, return_all=True)]
                count_unlock = [torch.sum(_) for _ in P.split(unlock, return_all=True)] if P.global_size>1 else [ torch.sum(unlock) ]

                #print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ", residue, my_unlock, unlock)

            if torch.all(is_convg):
                find = True
                i_iter = i_iter - 1
                break

            eigval_ = P.split(eigval)[my_unlock].clone()
            ## Check block is saturated
            if fill_block:
                if sub_dim == nblock * neig:
                    vprint(f"Stop block expansion.", verbosity=verbosityLevel)
                    break
                else:
                    #if sub_dim + R.shape[1] <= nblock * neig:
                    if sub_dim + len(residue) <= nblock * neig:
                        pass
                    else:
                        cutBlock = nblock * neig - sub_dim
                        ii = torch.argsort(residue[~is_convg], descending=False)
                        my_ii = ii[torch.logical_and( sum(count_unlock[:P.rank])<=ii, ii<sum(count_unlock[:P.rank+1]) )]
                        #print("fill block\n",i_iter, "\n",ii)
                        R = R[:, (my_ii < cutBlock) ]
                        eigval_ = eigval_[my_ii<cutBlock]
                        #print(i_iter, "\n", R)
            ## Preconditioning
            R = preconditioner(R, A, eigval_, i_scf_iter)
            del eigval_

#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (2): {et-st} s', flush=True)
#            st = time.time()

            ## Orthonormalize R
            merge_R = P.merge(R)
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (3-!): {et-st} s', flush=True)
#            st = time.time()
            merge_R = merge_R- P.all_reduce( U@ ( (U.conj().T)@merge_R ))
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (3-@): {et-st} s', flush=True)
#            st = time.time()
            merge_R = _orthonormalize(merge_R, method=ortho)
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (3-#): {et-st} s', flush=True)
#            st = time.time()

            R = P.split( merge_R )

#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (3-1): {et-st} s', flush=True)
#            st = time.time()

            AU_list.append(A @ R)
            if B is not None:
                BU_list.append(B @ R)
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (3-2): {et-st} s', flush=True)
#            st = time.time()

            ## Make A_sub_list
            for i in range(i_b + 1):
                A_sub_list.append(P.merge(merge_R.conj().T @ AU_list[i] ) )
                if B is not None:
                    B_sub_list.append(P.merge(merge_R.conj().T @ BU_list[i])  )

            sub_dim_list.append(merge_R.shape[1] )
            del merge_R
            sub_dim = sum(sub_dim_list)
            global_index = torch.cat([global_index, P.split( torch.arange(sum(sub_dim_list[:-1]), sub_dim, dtype=torch.int64, device=device ) )] )
            U = torch.hstack([U, R]).contiguous()
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (4): {et-st} s', flush=True)
#            st = time.time()
            
            vprint(f"block size: {(U.shape[0],sub_dim )}", verbosity=verbosityLevel)
            del R
            ## Construct subspace matrices
            enum = 0
            A_sub = torch.zeros((sub_dim, sub_dim), dtype=fp_type, device=device)
            B_sub = torch.zeros_like(A_sub) if B is not None else None
            for i in range(i_b + 1):
                for j in range(i + 1):  # only fill lower triangle
                    st_i = sum(sub_dim_list[:i])
                    ed_i = sum(sub_dim_list[: i + 1])
                    st_j = sum(sub_dim_list[:j])
                    ed_j = sum(sub_dim_list[: j + 1])
                    A_sub[st_i:ed_i, st_j:ed_j] = A_sub_list[enum]
                    if B is not None:
                        B_sub[st_i:ed_i, st_j:ed_j] = B_sub_list[enum]
                    enum += 1
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (5-1): {et-st} s', flush=True)
#            st = time.time()

            ## subspace diagonalization and update eigenpairs
            if B is None:
                eigval, C = torch.linalg.eigh(A_sub)
            else:
                eigval, C = gen_eigh(A_sub, B_sub)
            eigval, C = eigval[:neig], C[:, :neig]
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (5-2): {et-st} s', flush=True)
#            st = time.time()
            vprint(f"eigenvalue: {eigval}", verbosity=verbosityLevel)

            C_ = C[global_index].contiguous()

#            tmp = torch.cat([U, torch.hstack(AU_list), torch.hstack(BU_list)] ) if B is not None else torch.cat([U, torch.hstack(AU_list)] )
#            tmp = P.reduce_scatter( tmp@C_, dim=1 )
#            tmp = torch.split(tmp, [len(U), AU_list[0].size(0), BU_list[0].size(0) ] ) if B is not None else torch.split(tmp, [len(U), AU_list[0].size(0)] ) 
#            X, AX, BX = (tmp[0], tmp[1], tmp[2]) if B is not None else (tmp[0], tmp[1], X)
#            del tmp
            X =  P.reduce_scatter(                     U @ C_, dim=1) 
            AX = P.reduce_scatter( torch.hstack(AU_list) @ C_, dim=1)
            BX = P.reduce_scatter( torch.hstack(BU_list) @ C_, dim=1) if B is not None else X
        
            del C_

            i_b += 1
#            torch.cuda.synchronize(device)
#            et = time.time()
#            print(f'{i_iter} (5-3): {et-st} s, original')

        if find:
            if retHistory:
                eigHistory.append(eigval.to("cpu"))
                resHistory.append(residue.to("cpu"))
            break

    if find:
        vprint(
            f"* Block Davidson converged at (iter={i_iter}/{maxiter}, b={sub_dim}/{neig * nblock}) with residual norms: {residue}",
            verbosity=verbosityLevel,
        )
    else:
        vprint(
            f"Block Davidson do not converge with residual norms: {residue}",
            verbosity=verbosityLevel,
        )

    if retHistory:
        return eigval, X, eigHistory, resHistory
    else:
        return eigval, X


def _orthonormalize(X, method="cholesky"):
    if method == "cholesky":
        return _b_orthonormalize(None, X)[0]
    elif method == "qr":
        return torch.linalg.qr(X)[0].contiguous()
    else:
        raise NotImplementedError


def _b_orthonormalize(B, V, BV=None, retInvR=False, MP=False):
    normalization = V.abs().max(axis=0)[0] + torch.finfo(V.dtype).eps
    V = V / normalization
    if BV is None:
        if B is not None:
            BV = B(V)
        else:
            BV = V  # Shared data!!!
    else:
        BV = BV / normalization

    if V.is_complex():
        SP, DP = torch.complex64, torch.complex128
    else:
        SP, DP = torch.float32, torch.float64

    if MP:
        if B is not None:
            V32, BV32 = V.to(SP), BV32.to(SP)
            VBV = torch.matmul(V32.T.conj(), BV32)
        else:
            V32 = V.to(SP)
            VBV = torch.matmul(V32.T.conj(), V32)
        VBV = VBV.to(DP)

    else:
        VBV = torch.matmul(V.T.conj(), BV)

    VBV = (VBV + VBV.T.conj()) / 2

    try:
        # decompose V.T@B@V into L@L.T. U = L.T
        LT = torch.linalg.cholesky(VBV, upper=True)
        LTinv = torch.linalg.inv(LT)
        V = torch.matmul(V, LTinv)
        # V' = V@Uinv -> V'BV'=Uinv.T.conj()@VBV@Uinv=I
        if B is not None:
            BV = torch.matmul(BV, LTinv)
        else:
            BV = None

    except Exception as e:
        print("Warning!!\n", e)
        V = None
        BV = None
        LTinv = None

    if retInvR:
        return V, BV, LTinv, normalization

    else:
        return V, BV


if __name__ =="__main__":
    A = torch.rand(100,100)
    A = A + A.T
    X = torch.rand(100,4)
    from gospel.ParallelHelper import ParallelHelper as P
    P.set(4,10)
    print(A)
    print( davidson( A, X) )
