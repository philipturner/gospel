import torch
import numpy as np
from abc import ABCMeta, abstractmethod
from itertools import product

#np.random.seed(0)
#torch.manual_seed(0)


class Eigensolver(metaclass=ABCMeta):
    """Eigensolver parent class"""

    def __init__(self):
        self._type = None
        self._preconditioner = None
        self._starting_vector = None
        self._starting_value = None
        return

    def check_residue(self, hamiltonian, eigval, eigvec):
        residue = np.empty_like(eigvec)
        for i_s, i_k in product(range(eigvec.shape[0]), range(eigvec.shape[1])):
            residue[i_s, i_k] = np.linalg.norm(
                eigvec[i_s, i_k].T * eigval[i_s, i_k]
                - hamiltonian[i_s, i_k].dot(eigvec[i_s, i_k].T),
                axis=0,
            )
            print("Eigenvalue / Residue")
            for eigval, r in zip(eigval[i_s, i_k], residue[i_s, i_k]):
                print(f"{eigval:.2E} :  {r:.2E}")
        return residue

    @abstractmethod
    def diagonalize(self, hamiltonian, i_iter=None):
        """abstract method for hamiltonian diagonalization"""
        pass

    def _initialize_guess(self, hamiltonian, orthogonalize=True):
        """Initialize guess vectors and values."""
        from gospel.ParallelHelper import ParallelHelper as P

        if self._starting_vector is None:
            nspins = hamiltonian.nspins
            nibzkpts = hamiltonian.nibzkpts
            nbands = hamiltonian.nbands 
            ngpts = hamiltonian.ngpts
            # S = hamiltonian.get_S()  # overlap operator

            #self._starting_value = -np.random.rand(nspins, nibzkpts, nbands).astype(  float  )
            self._starting_value = torch.rand(nspins, nibzkpts, nbands, dtype= torch.float64, device = P.get_device(hamiltonian.use_cuda )) 
            self._starting_vector = np.empty((nspins, nibzkpts), dtype=object)

            for i_s, i_k in product(range(nspins), range(nibzkpts)):
                dtype = hamiltonian.get_kinetic_operator(i_k).dtype

                #psi = torch.randn(ngpts, P.split_size(nbands) , dtype=dtype, device=P.get_device(hamiltonian.use_cuda ) )
                psi_ = torch.randn(P.split_size(ngpts), nbands, dtype=dtype, device = P.get_device(hamiltonian.use_cuda) )
                psi_ = parallel_orthonormalize(psi_)
                psi = P.redistribute(psi_, dim0=0, dim1=1)
                del psi_
                self._starting_vector[i_s, i_k] = psi
                # psi = torch.randn(ngpts, nbands, dtype=hamiltonian[i_s, i_k].dtype)
                # psi = torch.linalg.qr(psi)[0]

        return

    @property
    def type(self):
        return self._type

    @property
    def preconditioner(self):
        return self._preconditioner

    @preconditioner.setter
    def preconditioner(self, preconditioner):
        self._preconditioner = preconditioner
        return



def gen_eigh(A, B=None):
    """
    Generalized eigenvalue problem solver for a complex Hermitian or real symmetric
    matrix, where B is positive-definite matrix. Solve :math:`Ax=\lambda Bx`, with
    cholesky decomposition :math:`L^{-1}AL^{*-1}\ L^*x=\lambda x\ s.t B=LL^*`
    When B is not defined, it is assumed identity matrix.

    :type  A: torch.Tensor
    :param A:
        tensor of shape (n,n), hermitian or real symmetric matrix
    :type B: torch.Tensor, optional
    :param B:
        tensor of shape (n,n), hermitian or real symmetric positive definite matrix.
        Assumed identity matrix of same shape of A if None. Default: None.
    :rtype: tuple
    :return:
        tuple of eigenvalue tensor and eigenvector tensor
    """
    if B is None:
        val, vec = torch.linalg.eigh(A)
    else:
        L = torch.linalg.cholesky(B)
        Linv = torch.linalg.inv(L)
        _A = (Linv @ A) @ Linv.conj().T
        val, vec = torch.linalg.eigh(_A)
        vec = Linv.conj().T @ vec
    return val, vec

def parallel_orthonormalize(X_):
    from gospel.ParallelHelper import ParallelHelper as P
    """
    orthogonalize X_ which is distributed along 0th axis 
    """
    normalization = P.all_reduce(X_.abs().max(axis=0)[0], op=torch.distributed.ReduceOp.MAX ) + torch.finfo(X_.dtype).eps
    X_ = X_ / normalization

    cov = P.all_reduce(X_.T.conj()@X_) 
    cov = (cov + cov.T.conj())/2

    LT = torch.linalg.cholesky( cov , upper=True)
    LTinv = torch.linalg.inv(LT)
    return  torch.matmul(X_, LTinv)
