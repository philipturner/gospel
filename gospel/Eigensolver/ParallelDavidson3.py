from itertools import product
import numpy as np
import torch
from math import sqrt
from gospel.LinearOperator import LinearOperator, aslinearoperator
from gospel.Eigensolver.Eigensolver import Eigensolver, gen_eigh, parallel_orthonormalize
from gospel.util import timer
import time
from gospel.ParallelHelper import ParallelHelper as P

torch.backends.cuda.matmul.allow_tf32 = False
print(
    f"torch.backends.cuda.matmul.allow_tf32 : {torch.backends.cuda.matmul.allow_tf32}"
)
torch.manual_seed(0)


def vprint(*args, verbosity=True):
    if verbosity:
        for arg in args:
            print(arg, end="")
        print()
    else:
        pass


class Davidson(Eigensolver):
    """Block Davidson method. (https://arxiv.org/abs/0906.2569)

    :type maxiter: int, optional
    :param maxiter:
        the maximum number of iterations, defaults to 20
    :type nblock: int, optional
    :param nblock:
        the maximum size of block, defaults to 3
    :type convg_tol: float or None, optional
    :param convg_tol:
        tolerance of convergence, defaults to None.
        ('convg_tol' is set to '(dimension) * 10**(-15/2)' as a default value.)
    :type locking: bool
    :param locking:
        whether locking converged eigenvectors or not
    :type verbosity: int, optional
    :param verbosity:
        verbose level (0 or 1), defaults 1
    :type fill_block: bool, optional
    :param fill_block:
        defaults to True
    """

    def __init__(
        self,
        maxiter=5,
        nblock=2,
        convg_tol=None,
        locking=True,
        verbosity=0,
        fill_block=True,
        dynamic=False,
        dynamic_tol=None,
        dynamic_maxiter=None,
    ):
        super().__init__()
        self._type = "davidson"
        self.__maxiter = maxiter
        self.__convg_tol = convg_tol
        self.__nblock = nblock
        self.__locking = locking
        self.__verbosity = verbosity
        self.__fill_block = fill_block

        self.__dynamic = dynamic
        self.__dynamic_tol = dynamic_tol
        self.__dynamic_maxiter = dynamic_maxiter
        return

    def __str__(self):
        s = str()
        s += f"\n===================== [ Eigensolver(Davidson) ] ===================="
        s += f"\n* convg_tol       : {self.__convg_tol}"
        s += f"\n* maxiter         : {self.__maxiter}"
        s += f"\n* nblock          : {self.__nblock}"
        s += f"\n* locking         : {self.__locking}"
        s += f"\n* verbosity       : {self.__verbosity}"
        s += f"\n* fill_block      : {self.__fill_block}"
        s += f"\n* dynamic         : {self.__dynamic}"
        s += f"\n* dynamic_tol     : {self.__dynamic_tol}"
        s += f"\n* dynamic_maxiter : {self.__dynamic_maxiter}"
        s += f"\n====================================================================\n"
        return str(s)

    @timer
    def diagonalize(self, hamiltonian, i_scf_iter=None):
        """Diagonalize hamiltonians corresponding each spin and k-points.

        :type  hamiltonian: gospel.Hamiltonian
        :param hamiltonian:
            Hamiltonian class object
        :type  i_scf_iter: int or None, optional
        :param i_scf_iter:
            index of SCF iteration

        :rtype: (np.ndarray, np.ndarray)
        :return:
            eigenvalues and eigenvectors,
            eigval.shape=(nspins, nibzkpts, nbands)
            eigvec.shape=(nspins, nibzkpts)---(nbands, ngpts)
        """
        self._initialize_guess(hamiltonian) 

        ## Solve eigenvalue problem
        eigval = torch.zeros_like(self._starting_value)
        eigvec = np.zeros_like(self._starting_vector)  # eigvec.dtype = object
        for i_s, i_k in product(range(eigvec.shape[0]), range(eigvec.shape[1])):
            A = hamiltonian[i_s, i_k]
            solve_options = {
                "A": A,
                "X": self._starting_vector[i_s, i_k].to(eigval.device),
                "B": hamiltonian.get_S(device=eigval.device),
                "preconditioner": self.preconditioner,
                "tol": self.__convg_tol,
                "maxiter": self.__maxiter,
                #"maxiter": self.__maxiter if i_scf_iter!=0 else self.__maxiter+5,
                "nblock": self.__nblock,
                "locking": self.__locking,
                "fill_block": self.__fill_block,
                # "dynamic": self.__dynamic,
                # "dynamicTol": self.__dynamic_tol,
                # "dynamicMaxiter": self.__dynamic_maxiter,
                "verbosityLevel": self.__verbosity,
                "i_scf_iter": i_scf_iter, 
            }
            val, vec = davidson(**solve_options)
            #val, vec = val.cpu(), vec.cpu()
            eigval[i_s, i_k] = val
            eigvec[i_s, i_k] = vec.T  ## memory copy.. (not eifficient code)

            ## update starting vectors and values
            self._starting_value[i_s, i_k] = val #eigval[i_s, i_k]
            self._starting_vector[i_s, i_k] = vec
        return eigval, eigvec

    @property
    def convg_tol(self):
        return self.__convg_tol

    @convg_tol.setter
    def convg_tol(self, inp):
        self.__convg_tol = inp
        return

    @property
    def dynamic(self):
        return self.__dynamic

def davidson(
    A,
    X,
    B=None,
    preconditioner=None,
    tol=1e-4,
    maxiter=20,
    nblock=2,
    locking=True,
    fill_block=True,
    verbosityLevel=0,
    ortho="cholesky",
    retHistory=False,
    i_scf_iter = 0,
):
    """Block Davidson eigenvalue problem solver.

    :type A: LinearOperator
    :param A:
        Hamiltonian operator
    :type X: torch.Tensor
    :param X:
        guess eigenvectors
    :type B: LinearOperator or None, optional
    :param B:
        overlap matrix for generalized eigenvalue problem
    :type M: LinearOperator or None, optional
    :param M:
        precondition operator
    :type tol: float, optional
    :param tol:
        convergence tolerance, defaults to 1e-4.
    :type maxiter: int, optional
    :param maxiter:
        the maximum number of iterations, defaults to 20
    :type nblock: int, optional
    :param nblock:
        the maximum size of block, defaults to 2
    :type locking: bool, optional
    :param locking:
        whether locking converged eigenvectors or not, defaults to True
    :type dynamic: bool, optional
    :param dynamic:
        whether using dynamic precision method or not, defaults to False
    :type dynamicTol: float, optional
    :param dynamicTol:
        convergence tolerance for dynamic precision
        ('dynamicTol' is set to '(dimension) * 10**(-15/4)' as a default value.)
    :type dynamicMaxiter: int, optional
    :param dynamicMaxiter:
        the maximum number of iterations for dynamic precision, defaults to 10
    :type verbosityLevel: int, optional
    :param verbosityLevel:
        verbosity level, defaults to 0
    :type ortho: str, optional
    :param ortho:
        orthogonalize method, options=["qr", "cholesky"], defaults to "cholesky"

    :rtype: tuple[torch.Tensor]
    :return:
        eigenvalues and eigenvectors
    """
    # all  : residue, A_sub_list, B_sub_list, eigval, C, is_convg,
    # split: AX, BX, R, is_convg, U, X, my_unlock, my_convg
    device = X.device
    fp_type = X.dtype
    A = aslinearoperator(A)
    B = aslinearoperator(B)
    #M = aslinearoperator(M)
    ## initializatioin
    st = time.time()
    X_  = P.redistribute(X)
    X_  = parallel_orthonormalize(X_)
    X = P.redistribute(X_, dim0=0, dim1=1)
    AX_ = P.redistribute( A @ X )

    neig = X_.shape[-1]
    P.synchronize(device)
    et = time.time()
    print(f'(A@X and redistribute X and AX): {et-st} s')
    st = time.time()
    A_sub = P.all_reduce(X_.T.conj()@ AX_)

    if B is None:
        BX = X  # shared data
        eigval, C = torch.linalg.eigh(A_sub)
    else:
        BX_ = P.redistribute( B @ X )
        B_sub = P.mall_reduce(X_.T.conj()@ BX_ )
        eigval, C = gen_eigh(A_sub, B_sub)
    P.synchronize(device)
    et = time.time()
    print(f'(initial subspace construction and diagonalization): {et-st} s')
    st = time.time()

    eigval, C = eigval[:neig], C[:, :neig]
    P.synchronize(device)
    et = time.time()
    print(f'(0-1): {et-st} s')
    st = time.time()

    vprint("-*" * 30, "i_iter=1", verbosity=verbosityLevel)
    vprint(f"eigenvalue: {eigval}", verbosity=verbosityLevel)

    X_  = X_ @ C 
    AX_ = AX_ @ C
    BX_ = BX_ @ C if B is not None else X_

    P.synchronize(device)
    et = time.time()
    print(f'(Rotate X, AX, and BX): {et-st} s')
    
    # Save convergence history
    eigHistory = []
    resHistory = []

    ## Start iteration
    find = False
    sub_dim = 0

    unlock = torch.full((neig,), True, device=device)
    my_unlock  = P.split(unlock.clone())
    preconditioner.reset_num_called()

    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n', X)
    for i_iter in range(2, maxiter + 1):
        vprint("\n" + "=*" * 50, f"i_iter={i_iter}", verbosity=verbosityLevel)
        st = time.time()
        X = P.redistribute(X_, dim0=0, dim1=1)
        U_  = X_
        AU_list = [AX_]
        P.synchronize(device)
        et = time.time()
        print(f'{i_iter} (redistribute X_ ): {et-st} s', flush=True)
        st = time.time()
        A_sub_list = [P.all_reduce(X_.conj().T@AX_) ]

        if B is not None:
            BU_list = [BX_]
            B_sub_list = P.all_reduce(X_.conj().T@BX_)

        P.synchronize(device)
        et = time.time()
        print(f'{i_iter} (X.H@AX & X.H@BX): {et-st} s', flush=True)

        ## Start block expansion
        sub_dim = A_sub_list[-1].shape[-1]  # dimension of the subspace
        sub_dim_list = [ sub_dim ]
        i_b = 1  # the number of expansions

        ## current rank's index for U and C
#        global_index    =  P.split(torch.arange(C.size(-1), dtype=torch.int64, device=device ))

        while True:
            ## Check the number of expansions
            if fill_block:
                if sub_dim == nblock * neig:
                    vprint(f"Stop block expansion.", verbosity=verbosityLevel)
                    break
            else: 
            #if not fill_block:
                if i_b == nblock:
                    vprint(
                        f"Stop block expansion. ({i_b} by {i_b})",
                        verbosity=verbosityLevel,
                    )
                    break

            count_unlock = [torch.sum(_) for _ in P.split(unlock, return_all=True)] if P.global_size>1 else [ torch.sum(unlock) ]
            st = time.time()
            ## Calculate residual
            R_ = AX_[:, unlock ] - eigval[unlock].unsqueeze(0) * BX_[:, unlock]
            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (AX-\lambda BX): {et-st} s', flush=True)
            print("RRRRRR!!!!!\n", R_)
            st = time.time()
            residue = torch.sqrt( P.all_reduce ( torch.sum(R_**2, dim=0) ) )

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (calculate residue): {et-st} s', flush=True)
            st = time.time()

            my_residue = P.split(residue)

            vprint(f"residual norms: {residue}", verbosity=verbosityLevel)
            if retHistory and i_b == 1:
                eigHistory.append(eigval.to("cpu"))
                resHistory.append(residue.to("cpu"))

            ## Check convergence and lock converged states
            is_convg = residue < tol
            my_convg = my_residue <tol
            if locking:
                R_ = R_[:,  ~is_convg  ] # update R

                unlock[unlock.clone()] =  ~is_convg  #update my_unlock
                my_unlock[my_unlock.clone()] =  ~my_convg  #update my_unlock

                count_unlock = [torch.sum(_) for _ in P.split(unlock, return_all=True)] if P.global_size>1 else [ torch.sum(unlock) ]

            my_unlock_eigval = P.split(eigval)[my_unlock].clone()

            if torch.all(is_convg):
                find = True
                i_iter = i_iter - 1
                break

            ## Check block is saturated
            if fill_block:
                if sub_dim == nblock * neig:
                    vprint(f"Stop block expansion.", verbosity=verbosityLevel)
                    break
                else:
                    #if sub_dim + R.shape[1] <= nblock * neig:
                    if sub_dim + len(residue) <= nblock * neig:
                        pass
                    else:
                        cutBlock = nblock * neig - sub_dim
                        ii = torch.argsort(residue[~is_convg], descending=False)
                        my_ii = ii[torch.logical_and( sum(count_unlock[:P.rank])<=ii, ii<sum(count_unlock[:P.rank+1]) )]
                        #print("fill block\n",i_iter, "\n",ii)
                        #R = R[:, (my_ii < cutBlock) ]
                        R_ = R_[:, ii < cutBlock ]
                        my_unlock_eigval = my_unlock_eigval[my_ii<cutBlock]
                        #print(i_iter, "\n", R)
            #del my_unlock_eigval

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (locking & fill block): {et-st} s', flush=True)
            st = time.time()
            ## Preconditioning
            R = preconditioner(P.redistribute(R_, dim0=0, dim1=1), A, my_unlock_eigval, i_scf_iter)

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (preconditioning): {et-st} s', flush=True)
            st = time.time()

            ## project out U component from R
            R_ = P.redistribute(R)
            R_ = R_-  U_ @  P.all_reduce( U_.conj().T @ R_ )

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (R-U@U.H@R): {et-st} s', flush=True)
            st = time.time()

            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n',P.all_reduce(R_.T.conj()@R_))
            # orthogonalize R
            R_ = parallel_orthonormalize(R_)
            R = P.redistribute(R_, dim0=0, dim1=1)

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (ortogonalize R): {et-st} s', flush=True)
            st = time.time()

            AU_list.append(P.redistribute(A @ R) )
            if B is not None:
                BU_list.append(P.redistribute(B @ R) )

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (A@R & B@R): {et-st} s', flush=True)
            st = time.time()

            ## Make A_sub_list
            for i in range(i_b + 1):
                A_sub_list.append(P.all_reduce(R_.conj().T @ AU_list[i] ) )
                if B is not None:
                    B_sub_list.append(P.all_reduce(R_.conj().T @ BU_list[i])  )

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (R.H@AU & R.H@BU): {et-st} s', flush=True)
            st = time.time()

            sub_dim_list.append(R_.shape[1] )

            sub_dim = sum(sub_dim_list)
#            global_index = torch.cat([global_index, P.split( torch.arange(sum(sub_dim_list[:-1]), sub_dim, dtype=torch.int64, device=device ) )] )
            U_ = torch.hstack([U_, R_]).contiguous()
            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (stack U): {et-st} s', flush=True)
            st = time.time()
            
            del R, R_
            ## Construct subspace matrices
            enum = 0
            A_sub = torch.zeros((sub_dim, sub_dim), dtype=fp_type, device=device)
            B_sub = torch.zeros_like(A_sub) if B is not None else None
            for i in range(i_b + 1):
                for j in range(i + 1):  # only fill lower triangle
                    st_i = sum(sub_dim_list[:i])
                    ed_i = sum(sub_dim_list[: i + 1])
                    st_j = sum(sub_dim_list[:j])
                    ed_j = sum(sub_dim_list[: j + 1])
                    A_sub[st_i:ed_i, st_j:ed_j] = A_sub_list[enum]
                    if B is not None:
                        B_sub[st_i:ed_i, st_j:ed_j] = B_sub_list[enum]
                    enum += 1
            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (subspace matrix construction): {et-st} s', flush=True)
            st = time.time()

            ## subspace diagonalization and update eigenpairs
            if B is None:
                eigval, C = torch.linalg.eigh(A_sub)
            else:
                eigval, C = gen_eigh(A_sub, B_sub)
            eigval, C = eigval[:neig], C[:, :neig]
            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (dense matrix diagonalization): {et-st} s', flush=True)
            st = time.time()
            vprint(f"eigenvalue: {eigval}", verbosity=verbosityLevel)

#            C_ = C[global_index].contiguous()
#            tmp = torch.cat([U, torch.hstack(AU_list), torch.hstack(BU_list)] ) if B is not None else torch.cat([U, torch.hstack(AU_list)] )
#            tmp = P.reduce_scatter( tmp@C_, dim=1 )
#            tmp = torch.split(tmp, [len(U), AU_list[0].size(0), BU_list[0].size(0) ] ) if B is not None else torch.split(tmp, [len(U), AU_list[0].size(0)] ) 
#            X, AX, BX = (tmp[0], tmp[1], tmp[2]) if B is not None else (tmp[0], tmp[1], X)
#            del tmp
            X_ =   U_@C
            AX_ = torch.hstack(AU_list) @ C
            BX_ = torch.hstack(BU_list) @ C if B is not None else X_

            P.synchronize(device)
            et = time.time()
            print(f'{i_iter} (Rotate X, AX, and BX): {et-st} s')
        
#            del C_


            i_b += 1

        if find:
            if retHistory:
                eigHistory.append(eigval.to("cpu"))
                resHistory.append(residue.to("cpu"))
            break

    if find:
        vprint(
            f"* Block Davidson converged at (iter={i_iter}/{maxiter}, b={sub_dim}/{neig * nblock}) with residual norms: {residue}",
            verbosity=verbosityLevel,
        )
    else:
        vprint(
            f"Block Davidson do not converge with residual norms: {residue}",
            verbosity=verbosityLevel,
        )

    if retHistory:
        return eigval, P.redistribute(X_, dim0=0, dim1=1), eigHistory, resHistory
    else:
        return eigval, P.redistribute(X_, dim0=0, dim1=1)




if __name__ =="__main__":
    A = torch.rand(100,100)
    A = A + A.T
    X = torch.rand(100,4)
    from gospel.ParallelHelper import ParallelHelper as P
    P.set(4,10)
    print(A)
    print( davidson( A, X) )
