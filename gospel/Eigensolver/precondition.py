import torch
import numpy as np
from gospel.LinearOperator import LinearOperator
from gospel.Poisson.ISF_solver import ISF_solver
from gospel.util import timer
from gospel.util import to_cuda
from gospel.ParallelHelper import ParallelHelper as P
torch.manual_seed(0)

def create_preconditioner(precond_type=None, grid=None, use_cuda=False):
    """Create Preconditioner object from input parameters.

    :type  precond_type: str or None, optional
    :param precond_type:
        type of preconditioner, available type=[None, "jacobi", "filter", "poisson", "gapp"]
    :type  grid: gospel.Grid or None, optional
    :param grid:
        Grid object

    *Example*
    >>> grid = Grid(atoms, gpts=(40, 40, 40))
    >>> precond = create_preconditioner("poisson", grid)
    """
#    if precond_type == "jacobi":
#        return PreJacobi()
    if isinstance(precond_type, list):
        assert all( [ isinstance(item, tuple) for item in precond_type] ), "If the type of precond_type is list, its item should be tuple "
        return MergePreconditioner(precond_type, grid, use_cuda)

    if precond_type == "poisson":
        return PrePoisson(grid,use_cuda=use_cuda)
    elif precond_type == "gapp":
        return PrePoisson(grid, t_sample=[[1.38], [2.55], 11.96], use_cuda=use_cuda)
    elif precond_type == "filter":
        return PreFilter(grid, use_cuda=use_cuda)
    elif precond_type == "inverse":
        return InverseHamiltonian( use_cuda=use_cuda)
    elif precond_type is None:
        return Preconditioner(None, use_cuda)
    else:
        raise NotImplementedError(f"{precond_type} is not available type.")


class Preconditioner:
    """parent class of preconditioners to accelerate convergence of iterative diagonalization.

    :type  precond_type: str or None, optional
    :param precond_type:
        available precondition types, defaults to None
        available precond_type
        - 'jacobi': inverse of diagonal elements of Hamiltonian matrix
        - 'poisson': solution of poisson equation of guess eigenvectors
        - 'filter': .
    """

    def __init__(self, precond_type, use_cuda):
        assert precond_type in [
            "jacobi",
            "poisson",
            "filter",
            "GKP",
            "inverse",
            'merge',
            None,
        ], f"{precond_type} is not supported precondition type."
        self._precond_type = precond_type
        self._device =  P.get_device(use_cuda)
        self.use_cuda = use_cuda
        self.num_called = 0 # this count number of calls in one scf step 
        return

    def __str__(self):
        s = str()
        s += "\n========================= [ Preconditioner ] ========================"
        s += f"\n* type  : {self._precond_type}"
        s += "\n=====================================================================\n"
        return str(s)

    def call(self, residue):
        return residue

    @timer
    def __call__(self, residue, H, eigval, i_iter):
        self.num_called+=1       
        if(self._precond_type=='inverse'):
            residue = self.call(residue, H, eigval)
        elif(self._precond_type=='merge'):
            residue = self.call(residue, H, eigval, i_iter)
        else:
            residue = self.call(residue)
        #print("preconditioner residue norm: ", torch.linalg.norm(residue, dim=0) )
        return residue
    def reset_num_called(self):
        self.num_called=0

#    def set_device(self, device=None):
#        self._device = device
#        return

#    @property
#    def device(self):
#        return self._device


class MergePreconditioner(Preconditioner):
    def __init__(self, list_precond_types, grid, use_cuda=False):
        super().__init__("merge", use_cuda)
        self.preconditioners = []
        self.iterations = []
        for precond_type, num_iter in list_precond_types:
            self.preconditioners.append( create_preconditioner(precond_type, grid, use_cuda) )
            self.iterations.append(num_iter)
        self.iterations = np.cumsum(np.array(self.iterations))
    def call(self, residue, H, eigval, i_scf_iter):
        iterations = self.iterations.copy() + (15 if i_scf_iter==0 else 0)

        try: 
            idx = iterations.tolist().index( min( iterations[(iterations-self.num_called) >0]  ) )
        except ValueError:
            idx=-1

        return self.preconditioners[idx](residue, H, eigval, i_scf_iter)

        
class GKP(Preconditioner):
    """Gaussian Kernel Preconditioner"""

    def __init__(self, grid, t_sample1, t_sample2, nextra, use_cuda=False):
        super().__init__("GKP", use_cuda)
        self.__grid = grid
        self.__solver1 = ISF_solver(grid, t_sample1, fp="DP", device=self._device)
        self.__solver2 = ISF_solver(grid, t_sample2, fp="DP", device=self._device)
        self.__nextra = nextra  # the number of extra states
        self.linear_op = None
        return

    def call(self, residue):
        if(self.linear_op is None):
            ps1 = self.__solver1
            ps2 = self.__solver2
    
            @timer
            def f(x):
                x = x.T  # shape=(nbands, ngpts)
                x1 = x[:-self.__nextra]
                x2 = x[-self.__nextra:]
                retval = torch.zeros_like(x)
                if x.dtype in [torch.complex64, torch.complex128]:
                    retval[:-self.__nextra] = (
                        ps1.compute_potential(x1.real) + ps1.compute_potential(x1.imag) * 1j
                    )
                    retval[-self.__nextra:] = (
                        ps2.compute_potential(x2.real) + ps2.compute_potential(x2.imag) * 1j
                    )
                elif x.dtype in [torch.float32, torch.float64]:
                    # retval = ps.compute_potential(x)
                    retval[:-self.__nextra] = ps1.compute_potential(x1)
                    retval[-self.__nextra:] = ps2.compute_potential(x2)
                else:
                    raise TypeError(f"x.dtype={x.dtype} is inappropriate.")
                return 2.0 * retval.T
    
            shape = (self.__grid.ngpts, self.__grid.ngpts)
            self.linear_op=  LinearOperator(shape, f)
        return linear_op@residue
#    def set_device(self, device=None):
#        self.__solver.set_device(device)
#        self._device = device
#        return


class PrePoisson(Preconditioner):
    """Poisson Preconditioner

    :type  grid: gospel.Grid or None, optional
    :param grid:
        Grid class object, defaults to None
    :type  t_sample: list or None
    :param t_sample:
        list of t values and weights and t_delta
    """

    def __init__(self, grid, t_sample=None, use_cuda=False):
        super().__init__("poisson", use_cuda)
        self.__grid = grid
        self.__solver = ISF_solver(grid, t_sample, fp="DP", device= self._device)

        # self.__solver.initialize(use_MP=True)
        self.linear_op = None
        return

    def __str__(self):
        s = str()
        s += "\n========================= [ Preconditioner ] ========================"
        s += f"\n* type  : poisson"
        s += self.__solver.__str__()
        s += "\n=====================================================================\n"
        return str(s)

    def call(self, residue):
        """Poisson filter preconditioning.

        :rtype: LinearOperator
        :return:
            precondition operator
        """
        if self.linear_op  is None:
            ps = self.__solver
    
            @timer
            def f(x):
                x = x.T  # x.T.shape=(nbands, ngpts)
                if x.dtype in [torch.complex64, torch.complex128]:
                    retval = (
                        ps.compute_potential(x.real) + ps.compute_potential(x.imag) * 1j
                    )
                elif x.dtype in [torch.float32, torch.float64]:
                    retval = ps.compute_potential(x)
                else:
                    raise TypeError(f"x.dtype={x.dtype} is inappropriate.")
                return 2.0 * retval.T
    
            shape = (self.__grid.ngpts, self.__grid.ngpts)
            self.linear_op = LinearOperator(shape, f)
        return self.linear_op@residue

#    def set_device(self, device=None):
#        self.__solver.set_device(device)
#        self._device = device
#        return

#class PreJacobi(Preconditioner):
#    """Jacobi Preconditioner"""
#
#    def __init__(self):
#        super().__init__("jacobi")
#        self.linear_op = None
#        return
#
#    def call(self, residue, H):
#        """Jacobi preconditioning. Inverse of diagonal elements
#
#        :type  H: torch.Tensor or LinearOperator
#        :param H:
#            Hamiltonian operator
#
#        :rtype: LinearOperator or None
#        :return:
#            precondition operator
#        """
#        if hasattr(H, "diagonal"):
#            f = lambda x: (1.0 / H.diagonal()) * x
#            return LinearOperator(H.shape, f, dtype=H.dtype)
#        else:
#            print(
#                "Warning: Preconditioner is not used. ('diagonal' is not defined in H.)"
#            )
#            return None


class PreFilter(Preconditioner):
    """Low-pass filter preconditioner"""

    def __init__(self, grid, alpha=0.5, use_cuda=False):
        super().__init__("filter", use_cuda)
        self.__grid = grid
        self.__alpha = alpha
        self.__filter = self.make_filter(grid)
        self.__kernel = "sparse"  # type of kernel, options=["sparse", "conv"]
        # convolution version (conv) will also be implemented.
        self.__filter = to_cuda(self.__filter,  self._device)
        self.linear_op = None
        return

    def __str__(self):
        s = str()
        s += "\n========================= [ Preconditioner ] ========================"
        s += f"\n* type  : filter"
        s += f"\n* alpha : {self.__alpha}"
        s += f"\n* kernel: {self.__kernel}"
        s += "\n=====================================================================\n"
        return str(s)

    def call(self, residue):
        """Low-pass filter preconditioning.

        :rtype: LinearOperator
        :return:
            precondition operator
        """
        if self.linear_op  is None:
            @timer
            def f(x):
                if x.dtype in [torch.complex64, torch.complex128]:
                    retval = self.__filter @ x.real + self.__filter @ x.imag * 1j
                elif x.dtype in [torch.float32, torch.float64]:
                    retval = self.__filter @ x
                else:
                    raise TypeError(f"x.dtype={x.dtype} is inappropriate.")
                return retval
    
            shape = (self.__grid.ngpts, self.__grid.ngpts)
            self.linear_op= LinearOperator(shape, f)
        return self.linear_op@residue

    def make_filter(self, grid):
        from scipy.sparse import identity, kron
        from gospel.util import scipy_to_torch_sparse

        fx = self.make_filter_axis(grid, 0)
        fy = self.make_filter_axis(grid, 1)
        fz = self.make_filter_axis(grid, 2)
        Ix = identity(grid.gpts[0])
        Iy = identity(grid.gpts[1])
        Iz = identity(grid.gpts[2])
        flter = kron(fx, kron(Iy, Iz)) + kron(Ix, kron(fy, Iz)) + kron(kron(Ix, Iy), fz)
        return scipy_to_torch_sparse(flter)

    def make_filter_axis(self, grid, axis):
        from scipy.sparse import diags
        import numpy as np

        pbc = grid.get_pbc()[axis]
        gpt = grid.gpts[axis]
        mat = diags(np.ones(gpt - 1), 1)
        if pbc:
            mat += diags(np.ones(1), gpt - 1)
        mat += mat.T
        mat *= (1 - self.__alpha) / 6
        mat += diags(np.ones(gpt) * self.__alpha / 3, 0)
        return mat

#    def set_device(self, device=None):
#        from gospel.util import to_cuda
#
#        self.__filter = to_cuda(self.__filter, device)
#        self._device = device
#        return

class InverseHamiltonian(Preconditioner):
    def __init__(self, use_cuda=False, rtol=1e-1, max_iter=300, alpha=1.0):
        super().__init__("inverse", use_cuda)
        self._precond_type = "inverse"
        self.prev_solution = None

        self.rtol = rtol
        self.alpha = alpha
        self.max_iter = max_iter
        return

    def call(self, residue, H, eigval):
        
        # if residue is small enough, skip inverse preconditioning because of numerical instability 
        # idx = torch.linalg.norm(residue, dim=0)>1e-4

        # clear prev_solution if size is changed
        if not (self.prev_solution is None):
            if ( self.prev_solution.size()!=residue.size()):
                self.prev_solution=None
        # residue =_cg_sparse_solve( H, eigval[idx], residue[:,idx],
        #                            x0= (None if self.prev_solution is None else self.prev_solution[:,idx] ),
        #                            max_iter = (1000 if self.num_called==0 else 1000)
        #                          )
        residue =_cg_sparse_solve(
            H,
            eigval,
            residue,
            x0= (None if self.prev_solution is None else self.prev_solution),
            max_iter=self.max_iter,
            tol=self.rtol,
            alpha=self.alpha,
        )
        self.prev_solution = residue.clone()
        return residue

#def _cg_sparse_solve(A, b, x0=None, max_iter=2000, tol=1e-6):
def _cg_sparse_solve(H, eigval, b, x0=None, max_iter=300, tol=1e-1, alpha=1.0):
    """
    Solve the sparse linear equation Ax = b using the conjugate gradient method.
    """

    # Compute squared norm of b
    # skip preconditioning when norm of b is less than criteria 
    # TODO: criteria optimization (now it is hard coded as 1e-2)

    # Initial guess for solution
    if x0 is None:
        # x0 = torch.rand_like(b)
        x0 = torch.randn_like(b)

    # Compute squared norm of b
    b_norm_sq = b.norm(dim=0) ** 2
    b_norm_sq = b_norm_sq.unsqueeze(0)

    # perturb = torch.randn_like(eigval) * b_norm_sq.squeeze() * alpha
    perturb = -abs(torch.randn_like(eigval)) * b_norm_sq.squeeze() * alpha
    # perturb = - b_norm_sq.squeeze() * alpha
    # perturb = 1e-3

    # excess_idx = abs(perturb) >= 10.0
    # perturb[excess_idx] = 10 * torch.randn_like(eigval[excess_idx])
    print(f"perturb={perturb}")
    eigval = eigval + perturb

    # Compute residual and initialize search direction
    r = b - (H@x0-eigval.unsqueeze(0)*x0)
    p = r.clone()


    is_convg = torch.zeros(   b.size(1) , dtype=torch.bool, device=b_norm_sq.device)
    for i in range(max_iter):
        # Compute matrix-vector product
        Ap = H@p[:,~is_convg]-eigval[~is_convg].unsqueeze(0)*p[:,~is_convg]

        # Compute step size
        alpha = torch.sum(r[:,~is_convg] ** 2, dim=0) / torch.sum(p[:,~is_convg] * Ap, dim=0)
        alpha = alpha.unsqueeze(0)

        # Update solution and residual
        x = x0[:,~is_convg] + alpha * p[:,~is_convg]
        r_new = r[:,~is_convg] - alpha * Ap
        #print(i," r_new norm", torch.linalg.norm(r_new, dim=0) )
        #print(i," b_norm_sq ", b_norm_sq.squeeze(0)            )

        # Update search direction
        beta = torch.sum(r_new ** 2, dim=0) / torch.sum(r[:,~is_convg] ** 2, dim=0)
        beta = beta.unsqueeze(0)

        p[:,~is_convg] = r_new + beta * p[:,~is_convg]

        # Update variables for next iteration
        r[:,~is_convg] = r_new
        x0[:,~is_convg] = x

#        print(0, i, r_new.norm(dim=0), "\n" , b_norm_sq[0][~is_convg])
        is_convg[~is_convg]= (r_new.norm(dim=0)  < tol * b_norm_sq[0][~is_convg] )

#        print(1, i, is_convg)
        # Check convergence
        if torch.all(is_convg):
            break

    # print(f"Debug: cg_iter={i}, r_norm={r_new.norm(dim=0)}")
    return -x0


if __name__ == "__main__":
    from ase import Atoms
    from gospel.Grid import Grid

    grid = Grid(Atoms(cell=[3, 4, 5]), gpts=(100, 100, 100))
    precond = PreFilter(grid)
    print(precond)
    flter = precond.make_filter(grid)
    print(flter)

    M = precond()

    nbands = 10
    R = torch.randn(grid.ngpts, nbands, dtype=torch.float64)
    R = M @ R
