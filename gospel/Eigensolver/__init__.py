from gospel.Eigensolver.CG import CG
from gospel.Eigensolver.Tucker import Tucker
from gospel.Eigensolver.Scipy import Scipy
from gospel.Eigensolver.Davidson import Davidson
from gospel.Eigensolver.ParallelDavidson3 import Davidson as ParallelDavidson
from gospel.Eigensolver.lobpcg import LOBPCG
#from gospel.Eigensolver.SSRR import SSRR


def create_eigensolver(params):
    """Create Eigensolver object from input parameters.

    :type  params: dict
    :param params:
        dictionary of eigensolver options

    *Example*
    >>> params={
    ...     "type": 'davidson',
    ...     "maxiter": 20,
    ...     "convg_tol": 1e-7,
    ... }
    >>> eigensolver = create_eigensolver(params)
    """
    supported_eigensolver = {
        "cg": CG,
        "scipy": Scipy,
        "tucker": Tucker,
        "davidson": Davidson,
        "parallel_davidson": ParallelDavidson,
        "lobpcg": LOBPCG,
        #"ssrr": SSRR,
    }
    if params is None:
        eigensolver = LOBPCG()
    elif isinstance(params, dict):
        _params = params.copy()
        eigensolver = supported_eigensolver[_params.pop("type").lower()](**_params)
    else:
        print(f"Custom eigensolver is used.")
        eigensolver = params
    return eigensolver
