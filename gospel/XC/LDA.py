import pylibxc
from gospel.XC.LibXC import LibXC


class LDA(LibXC):
    def __init__(self, func_name, spin="unpolarized"):
        self.__functional_type = "lda"
        self.spin = spin  # 'unpolarized' or 'polarized'
        self.func = pylibxc.LibXCFunctional(func_name, self.spin)
        self.__Vxc = "XC potential"  # will be computed from 'self.compute()'
        self.__Exc = "XC energy"  # will be computed from 'self.compute()'
        return

    def compute(self, grid, density):
        inp = {"rho": density.T.contiguous().cpu()}
        result = self.func.compute(inp)
        for key, val in result.items():
            result[key] = torch.from_numpy(val).to(density.device, non_blocking=True)

        self.__Vxc = self.compute_Vxc(result)
        self.__Exc = self.compute_Exc(result, density) * grid.microvolume
        return

    def compute_Vxc(self, result):
        r"""
        V_{xc, \alpha} = \frac{\partial f}{\partial \rho_{\alpha}}
                       = vrho_{\alpha}
        """
        return result["vrho"].T

    def compute_Exc(self, result, density):
        r"""
        E_{xc} = \int \epsilon \rho d\vec{r}
        """
        # depending on version of libxc, shape of result['zk'] varies
        # therefore, flatten result and density and perform dot product  Sunghwan Choi
        return (sum(density).reshape(-1) @ result["zk"].reshape(-1)).item()

    @property
    def Vxc(self):
        return self.__Vxc

    @property
    def Exc(self):
        return self.__Exc

    @property
    def functional_type(self):
        return self.__functional_type


if __name__ == "__main__":
    import torch

    func_name = "lda_x"
    print("func_name = ", func_name)
    spin = "unpolarized"
    lda = LDA(func_name, spin)

    class Grid:
        microvolume = 1.0

    grid = Grid()

    grid_size = 5
    if spin == "unpolarized":
        density = torch.zeros((1, grid_size))
    else:
        density = torch.zeros((2, grid_size))
    density[0] = torch.arange(0.1, 0.6, 0.1)
    print("density = \n", density)

    ref_Vxc = -((3 / torch.pi) ** (1 / 3)) * density ** (1 / 3)
    print("ref_Vxc = ", ref_Vxc)

    lda.compute(grid, density)
    print(f"Vxc = {lda.Vxc}")
    print(f"Exc = {lda.Exc}")
