from abc import ABCMeta, abstractmethod


class LibXC(metaclass=ABCMeta):
    """https://www.tddft.org/programs/libxc/manual/"""

    spin = 'unpolarized or polarized'
    func = 'LibXCFunctional class object'
    result = 'result of compute method in pylibxc'

    @abstractmethod
    def compute(self):
        pass

    @abstractmethod
    def compute_Vxc(self):
        pass

    @abstractmethod
    def compute_Exc(self):
        pass

    def get_describe(self):
        return self.func.describe()
