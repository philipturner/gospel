import torch
import numpy as np
import pylibxc
from gospel.XC.LibXC import LibXC
from gospel.FdOperators import gradient, divergence


def calc_sigma(grid, density, spin):
    if spin == "unpolarized":
        #sigma = torch.zeros(1, density.shape[1], dtype=density.dtype, device=density.device)
        dn = gradient(grid, density[0], False)
        sigma = (dn**2).sum(0, keepdim=True)
    else:  # spin == 'polarized'
        sigma = torch.zeros(3, density.shape[1], dtype=density.dtype, device=density.device)
        dn_a = gradient(grid, density[0], False)
        dn_b = gradient(grid, density[1], False)
        sigma[0] = (dn_a**2).sum(0)
        sigma[1] = (dn_a * dn_b).sum(0)
        sigma[2] = (dn_b**2).sum(0)
    return sigma


class GGA(LibXC):
    def __init__(self, func_name, spin="unpolarized"):
        self.__functional_type = "gga"
        self.spin = spin  # 'unpolarized' or 'polarized'
        self.func = pylibxc.LibXCFunctional(func_name, self.spin)
        self.__Vxc = "XC potential"  # will be computed from 'self.compute()'
        self.__Exc = "XC energy"  # will be computed from 'self.compute()'
        return

    def compute(self, grid, density):
        rho   = density.contiguous().cpu() 
        sigma = calc_sigma(grid, rho, self.spin)
        inp = {"rho": rho.T.contiguous(), "sigma": sigma.T.contiguous()} # both tensors should be in cpu
        result = self.func.compute(inp)
        for key, val in result.items():
            result[key] = torch.from_numpy(val).to(density.device, non_blocking=True)
        self.__Vxc = self.compute_Vxc(result, grid, density)
        self.__Exc = self.compute_Exc(result, grid, density)
        return

    def compute_Vxc(self, result, grid, density):
        r"""
        V_{\alpha} = \frac{\partial f}{\partial \rho_{\alpha}}
                     - 2 \nabla \cdot ( \frac{\partial f}{\partial \gamma_{\alpha \alpha}} \nabla \rho_{\alpha} )
                     - \nabla \cdot ( \frac{\partial f}{\partial \gamma_{\alpha \beta}} \nabla \rho_{\beta} )
        """
        vrho = result["vrho"].T
        vsigma = result["vsigma"].T

        Vxc = torch.zeros_like(density)
        if self.spin == "unpolarized":
            dn = gradient(grid, density[0])
            Vxc[0] = vrho[0] - divergence(grid, 2 * vsigma[0] * dn, False )
        else:  ## spin == 'polarized'
            dn_a = gradient(grid, density[0])
            dn_b = gradient(grid, density[1])
            tmp = vsigma[1] * dn_b
            Vxc[0] = vrho[0] - divergence(grid, 2 * vsigma[0] * dn_a + tmp, False)
            Vxc[1] = vrho[1] - divergence(grid, 2 * vsigma[2] * dn_a + tmp, False)
        return Vxc

    def compute_Exc(self, result, grid, density):
        r"""
        E_{xc} = \int \epsilon \rho d\vec{r}
        """
        return grid.integrate(density.sum(0) * result["zk"].T).item()

    @property
    def Vxc(self):
        return self.__Vxc

    @property
    def Exc(self):
        return self.__Exc

    @property
    def functional_type(self):
        return self.__functional_type


if __name__ == "__main__":
    func_name = "gga_x_pbe"
    spin = "unpolarized"
    gga = GGA(func_name, spin)
