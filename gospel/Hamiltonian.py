from itertools import product
import torch
import numpy as np
from gospel.LinearOperator import LinearOperator
from gospel.FdOperators import make_kinetic_op, calc_kinetic
from gospel.util import scipy_to_torch_sparse, to_cuda, timer
from gospel.ParallelHelper import ParallelHelper as P

class Hamiltonian:
    """Make, update, and diagonalize Hamiltonian.

    :type  nspins: int
    :param nspins:
        the number of spins, 1 for 'unpolarized', 2 for 'polarized'
    :type  nbands: int
    :param nbands:
        the number of bands
    :type  grid: gospel.Grid
    :param grid:
        Grid object
    :type  kpoint: gospel.Kpoint
    :param kpoint:
        Kpoint object
    :type  pp: gospel.Pseudopotential
    :param pp:
        Pseudopotential object
    :type  poisson_solver: gospel.Poisson.Poisson_solver
    :param poisson_solver:
        Poisson_solver object
    :type  xc_functional: gospel.XC.XC_Functional
    :param xc_functional:
        XC_Functional object
    :type  eigensolver: gospel.Eigensolver
    :param eigensolver:
        Eigensolver object
    :type  use_cuda: bool, optional
    :param use_cuda:
        whether to use CUDA to accelerate, defaults to False
    """

    def __init__(
        self,
        nspins,
        nbands,
        grid,
        kpoint,
        pp,
        poisson_solver,
        xc_functional,
        eigensolver,
        use_dense_kinetic=False,
        use_cuda=False,
    ):
        device = P.get_device(use_cuda)
        self.__nspins = nspins
        self.__nbands = nbands
        self.__nkpts = kpoint.nkpts
        self.__nibzkpts = kpoint.nibzkpts
        self.__kpts = kpoint.kpts
        self.__gpts = grid.gpts
        self.__ngpts = grid.ngpts
        self.__shape = (self.__ngpts, self.__ngpts)

        self.grid = grid
        self.kpoint = kpoint
        self.pp = pp
        self.poisson_solver = poisson_solver
        #self.poisson_solver.set_device(device)
        self.xc_functional = xc_functional
        self.eigensolver = eigensolver

        self.__use_dense_kinetic = use_dense_kinetic
        self.__use_cuda = use_cuda

        """
        H = T_s + V_H + V_xc + V_ext + V_NL,
        V_loc = V_H + V_xc + V_ext,
        where only V_H and V_xc are changed during SCF cycles.
        """
        ## kinetic operators for each k-point
        self.__T_s = self.make_kinetic_operators(grid, self.__kpts, use_dense_kinetic)
        ## Hartree potential
        self.__V_H = torch.zeros(self.__ngpts, dtype=torch.float64)
        ## XC potential
        self.__V_xc = torch.zeros(self.__nspins, self.__ngpts, dtype=torch.float64)
        ## external potential
        self.__V_ext = self.pp.V_ext.to(device)
        ## sum of local potentials
        self.__V_loc = torch.zeros(self.__nspins, self.__ngpts, dtype=torch.float64)

        if (self.__use_dense_kinetic):
#            # dense kinetic 
#            # tracing
#            test_inputs  = (torch.tensor(self.grid.gpts, dtype=torch.long),
#                            self.__T_s[0][0], self.__T_s[0][1], self.__T_s[0][2],
#                            torch.rand(self.__ngpts, nbands//P.global_size, device=device, dtype=torch.float64))
#    
#            self.traced_calc_kinetic = torch.jit.trace(calc_kinetic, test_inputs) 
            self.traced_calc_kinetic = calc_kinetic
        else:
            self.traced_calc_kinetic = calc_kinetic
        self.V_NL = None
        
    
#        import time
#        st = time.time()
#        for _ in range(1000):
#            self.traced_calc_kinetic(*test_inputs)
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(et-st)
#        st = time.time()
#        for _ in range(1000):
#            calc_kinetic(*test_inputs)
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(et-st)
#        st = time.time()
#        for _ in range(1000):
#            self.traced_calc_kinetic(*test_inputs)
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(et-st)
#        st = time.time()
#        for _ in range(1000):
#            calc_kinetic(*test_inputs)
#        torch.cuda.synchronize(device)
#        et = time.time()
#        print(et-st)
#        exit(-1)
        return

    @timer
    def calc_forces(self, density, deriv_density=True, deriv_comp=True):
        """Calculate atomic Hellmann-Feynman forces.

        :type  density: gospel.Density
        :param density:
            Density class object
        :type  deriv_density: bool
        :param deriv_density:
            options for calculation of forces. derivatives of density or potential.
        :rtype: torch.Tensor
        :return:
            Hellmann-Feynman forces, shape=(natoms, 3)
        """
        F = self.pp.calc_forces(
            density,
            #self.__V_H,
            self.__V_H.cpu().numpy(),
            deriv_density=deriv_density,
            deriv_comp=deriv_comp,
        )
        return F

    def construct_hamiltonian_matrix(self, i_s, i_k):
        """Construct Hamiltonian matrix. (scipy.sparse) """
        from gospel.util import torch_diag_sparse
        from Warning import DeprecationWarning
        raise DeprecationWarning('construct_hamiltonian_matrix function is deprecated')
        assert not self.__use_dense_kinetic
        T = self.__T_s[i_k]
        V_loc = torch_diag_sparse(self.__V_loc[i_s])

        if self.pp.has_nonlocal:
            V_NL = scipy_to_torch_sparse(self.pp.get_nonlocal_matrix(i_k))
            H = T + V_loc + V_NL
        else:
            H = T + V_loc
        non_zero_ratio = len(H.values()) / self.__ngpts**2 * 100
        print(f"Hamiltonian non-zero ratio (i_s={i_s}, i_k={i_k}) : {non_zero_ratio} %")
        return H

    def make_kinetic_operators(self, grid, kpts, use_dense_kinetic=False):
        """Make kinetic operators

        :rtype: list[tuple[torch.Tensor] or torch.Tensor]
        :return:
            list of kinetic operators for each k-point
        """
        T_s = []
        if use_dense_kinetic:
            for kpt in kpts:
                T_xx,T_yy,T_zz= make_kinetic_op(grid, kpt, False, False)
                T_xx = T_xx.to(P.get_device(self.__use_cuda))
                T_yy = T_yy.to(P.get_device(self.__use_cuda))
                T_zz = T_zz.to(P.get_device(self.__use_cuda))
    
                T_s.append(( T_xx, T_yy, T_zz ) )
        else:  # use sparse operator
            for kpt in kpts:
                T_s.append(make_kinetic_op(grid, kpt, combine=True).to(P.get_device(self.__use_cuda))  )
        return T_s

    def get_kinetic_operator(self, i_k, device=None, dtype=None):
        """Return kinetic operator corresponding to i_k-th k-point.

        :type  i_k: int
        :param i_k:
            index of k-point
        :type  device: torch.device or None, optional
        :param device:
            device
        :type  dtype: torch.dtype or None, optional
        :param dtype:
            data type of kinetic operator
        """
        if self.__use_dense_kinetic:
            T_xx = self.__T_s[i_k][0].to(dtype=dtype)
            T_yy = self.__T_s[i_k][1].to(dtype=dtype)
            T_zz = self.__T_s[i_k][2].to(dtype=dtype)
            func = lambda x: self.traced_calc_kinetic(torch.tensor(self.grid.gpts , dtype=torch.long), T_xx, T_yy, T_zz, x)
            #func = lambda x: calc_kinetic(nx,ny,nz, T_xx, T_yy, T_zz, x)
            return LinearOperator(self.__shape, func, dtype=T_xx.dtype)
        else:
            T_s = to_cuda(self.__T_s[i_k], device, dtype)
            func = lambda x: T_s @ x
            return LinearOperator(self.__shape, func, dtype=T_s.dtype)

    @timer
    def get_linear_operator(self, i_s, i_k, dtype=None):
        """Return Hamiltonian operator

        :type  i_s: int
        :param i_s:
            index of spin
        :type  i_k: int
        :param i_k:
            index of k-point

        :rtype: gospel.LinearOperator
        :return:
            Hamiltonian operator
        """
        
        device = P.get_device(self.__use_cuda)

        ## Make Hamiltonian operater
        T = self.get_kinetic_operator(i_k, device, dtype)
        V_loc = self.__V_loc[i_s].reshape(-1, 1).to( dtype= T.dtype)
        #V_loc = self.__V_loc[i_s].reshape(-1, 1).to(device= device, dtype= T.dtype)
        if self.pp.has_nonlocal:
            if(self.V_NL is None):
                self.V_NL = self.pp.get_nonlocal_op(i_k, use_cuda=self.__use_cuda, dtype=T.dtype)
                if(self.__use_cuda and self.pp.use_dense_proj):
                    #test_inputs  = torch.rand(self.ngpts, self.nbands, device=device, dtype=T.dtype)
                    #self.V_NL = torch.jit.trace(self.V_NL,test_inputs)
                    self.V_NL = self.V_NL
    
            #H = lambda x:  V_loc * x  # shchoi debugging
            H = lambda x: T @ x + V_loc * x + self.V_NL(x)

        else:
            H = lambda x: T @ x + V_loc * x
        H_op = LinearOperator(self.__shape, H, dtype=T.dtype)
        #H_op.device = device

        ## Set matvec32 for dynamic precision
        if self.eigensolver.dynamic:
            if T.dtype == torch.float64:
                sp_dtype = torch.float32
            elif T.dtype == torch.complex128:
                sp_dtype = torch.complex64
            else:
                raise TypeError

            # T_32 = to_cuda(T, dtype=sp_dtype)
            T_32 = self.get_kinetic_operator(i_k, device, sp_dtype)
            V_loc_32 = V_loc.to(sp_dtype)

            if self.pp.has_nonlocal:
                V_NL_32 = self.pp.get_nonlocal_op(
                    i_k, use_cuda=self.__use_cuda, dtype=sp_dtype
                )
                H_32 = lambda x: T_32 @ x + V_loc_32 * x + V_NL_32(x)
            else:
                H_32 = lambda x: T_32 @ x + V_loc_32 * x
            H_op.matvec32 = H_32
        else:
            pass
        return H_op

    def diagonalize(self, i_iter=None):
        """Diagonalize Hamiltonian

        :type  i_iter: int or None
        :param i_iter:
            i-th iteration of SCF

        :rtype: tuple[np.ndarray, torch.Tensor]
        :return:
            eigenvalues and eigenvectors
        """
        return self.eigensolver.diagonalize(self, i_iter)

    @timer
    def calc_and_print_energies(self, density, eigval, eigvec, occ):
        """Calculate and print total and each energy term

        :type  density: gospel.Density
        :param density:
            Density object
        :type  eigval: torch.Tensor
        :param eigval:
            eigenvalues, shape=(nspins, nibzkpts, nbands)
        :type  eigvec: np.ndarray[torch.Tensor]
        :param eigvec:
            eigenvectors, shape=(nspins, nibzkpts)---(nbands, ngpts)
        :type  occ: torch.Tensor
        :param occ:
            occupation number, shape=(nspins, nibzkpts, nbands)

        :rtype: float
        :return:
            Total energy with a.u.
        """

        from gospel.ParallelHelper import ParallelHelper as P
        ## Calculate E_{ext} and E_{NL}
        E_ext = self.pp.calc_external_energy(density)
        if self.pp.has_nonlocal:
            E_NL = self.pp.calc_nonlocal_energy(occ, eigvec, use_cuda=self.__use_cuda)
        else:
            E_NL = 0.0

        ## Calculate E_{NN}
        E_NN = self.pp.ion_ion_repulsion_energy

        ## Calculate E_{kinetic}
        device = eigvec[0,0].device
        dtype = eigvec[0,0].dtype
        E_kin = torch.zeros(1, device=device, dtype=dtype )
        #list_i_bands = P.split(torch.arange(self.__nbands, device=device))
        p_occ = P.split(occ).to(device=device, dtype=dtype)

        for i_k in range(self.__nibzkpts):
            kinetic_op = self.get_kinetic_operator(i_k, device=device)
            for i_s in range(self.__nspins):
                E_kin += torch.sum( p_occ[i_s, i_k].unsqueeze(0) * ( eigvec[i_s, i_k].conj().T * (kinetic_op @ eigvec[i_s, i_k].T ) ).real )
        E_kin = P.all_reduce(E_kin) 

#                for idx, i_band in enumerate(list_i_bands) :
#                #for i in range(self.__nbands):
#                    if abs(occ[i_s, i_k, i_band]) < 1e-7:
#                        continue
#                    val = eigvec[i_s, i_k][idx].conj() @ (kinetic_op @ eigvec[i_s, i_k][idx])
#    
#                    if val.dtype in [torch.complex64, torch.complex128]:
#                        assert abs(val.imag) < 1e-7
#                    E_kin += P.all_reduce(occ[i_s, i_k, i_band] * val.real)

        ## Calculate E_{Hartree}
        E_H = (
            self.poisson_solver.compute_energy(
                density.get_density(compensated=True), self.__V_H
            )
            if self.poisson_solver != None
            else 0.0
        )

        ## Calculate E_{xc}
        Exc = self.xc_functional.get_Exc()

        
        ## 임시 ##
        E_NN = E_NN.detach().cpu() if isinstance(E_NN, torch.Tensor) else torch.tensor(E_NN, dtype=dtype)
        E_NL = E_NL.detach().cpu() if isinstance(E_NL, torch.Tensor) else torch.tensor(E_NL, dtype=dtype)
        Exc  = Exc.detach().cpu()  if isinstance(Exc,  torch.Tensor) else torch.tensor(Exc , dtype=dtype)
        ################################################################################

        ## E_{tot} = E_{kinetic} + E_{ext} + E_{NL} + E_{H} + E_{NN} + E_{xc}
        E_total = E_kin + E_ext + E_NL + E_H + E_NN + Exc
        eigval_sum = (occ * eigval).sum((1, 2))

        ## print energies
        _round = lambda x: torch.round(x, decimals=10).item()
        print(f"\n{'':=<{12}} {'Energy (Hartree)'} {'':=<{14}}")
        print(f"|   {'Total Energy': <{20}}: {_round(E_total):<{17}}|")
        print("-" * 44)
        print(f"| * {'Ion-ion Energy': <{20}}: {_round(E_NN):<{17}}|")
        for i_s in range(len(eigval_sum)):
            print(
                f"| * {'Eigenvals sum for '+str(i_s): <{20}}: {_round(eigval_sum[i_s]):<{17}}|"
            )
        print(f"| * {'Hartree Energy': <{20}}: {_round(E_H):<{17}}|")
        print(f"| * {'XC Energy': <{20}}: {_round(Exc):<{17}}|")
        print(f"| * {'Kinetic Energy': <{20}}: {_round(E_kin):<{17}}|")
        print(f"| * {'External Energy': <{20}}: {_round(E_ext):<{17}}|")
        print(f"| * {'Non-local Energy': <{20}}: {_round(E_NL):<{17}}|")
        print("=" * 44)
        return E_total

    def __getitem__(self, indx):
        i_s, i_k = indx
        if i_s >= self.__nspins or i_k >= self.__nibzkpts:
            raise IndexError
        else:
            return self.get_linear_operator(i_s, i_k)
            ## construct Hamiltonian matrix
            # return self.construct_hamiltonian_matrix(i_s, i_k)
            # return scipy_to_torch_sparse(self.construct_hamiltonian_matrix(i_s, i_k))

    def update(self, density):
        """Calculate and update Hartree and XC potentials from the input density.

        :type  density: gospel.Density
        :param density:
            Density class object
        """
        self.__V_H = self.poisson_solver.compute_potential(
            density.get_density(compensated=True)
        )
        self.xc_functional.compute(density)  
        self.__V_xc = self.xc_functional.get_Vxc()
        # V_ext is already moved on the device in constructor 
        # V_H is computed on the device 
        self.__V_loc = self.__V_H + self.__V_xc + self.__V_ext 
        return

    def get_S(self, device=None):
        """Return overlap operator"""
        if self.pp.get_S() is None:
            return None
        else:
            S = LinearOperator(self.__shape, self.pp.get_S(device))
            return S

    @property
    def V_loc(self):
        return self.__V_loc

    @V_loc.setter
    def V_loc(self, inp):
        assert inp.shape == self.__V_loc.shape
        self.__V_loc = inp
        return

    @property
    def T_s(self, i_k=None):
        if i_k is None:
            return self.__T_s
        else:
            return self.__T_s[i_k]

    @property
    def shape(self):
        return self.__shape

    @property
    def ngpts(self):
        return self.__ngpts

    @property
    def gpts(self):
        return self.__gpts

    @property
    def nspins(self):
        return self.__nspins

    @property
    def nbands(self):
        return self.__nbands

    @property
    def nibzkpts(self):
        return self.__nibzkpts

    @property
    def kpts(self):
        return self.__kpts

    @property
    def use_cuda(self):
        return self.__use_cuda
