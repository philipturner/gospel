numpy>=1.20.1
scipy>=1.6.1
pylibxc2>=6.0.0
spglib>=1.16.1
ase>=3.21.1
--find-links https://download.pytorch.org/whl/torch_stable.html
torch==1.11.0+cu115
# torch==1.11.0+cu102
