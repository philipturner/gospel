# GOSPEL package
---------------------------------------------------
GOSPEL is an open-source Python package for real-space density functional theory (DFT) calculations.
GOSPEL has been implemented using the PyTorch framework, so GPU acceleration is readily available.

## Dependencies
---------------
* numpy>=1.20.1
* scipy>=1.6.1
* pylibxc2>=6.0.0
* spglib>=1.16.1
* ase>=3.21.1
* torch>=1.11.0

## How to install environment
-----------------------------
    pip install -r requirements.txt
    python setup.py install
