.. GOSPEL documentation master file.

Welcome to GOSPEL's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Examples:

   examples/test.md


.. toctree::
   :maxdepth: 2
   :caption: Source Code:

   gospel


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
