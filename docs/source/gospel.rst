gospel
================

.. automodule:: gospel
    :members:
    :undoc-members:
    :show-inheritance:

    calculator
    ----------

    .. automodule:: gospel.calculator
        :members:

    Grid
    ----------

    .. automodule:: gospel.Grid
        :members:

    scf
    ----------

    .. automodule:: gospel.scf
        :members:

    FD_Operators
    ----------

    .. automodule:: gospel.FD_Operators
        :members:

    Mixing
    ----------

    .. automodule:: gospel.Mixing
        :members:

    Occupation
    ----------

    .. automodule:: gospel.Occupation
        :members:

    .. automodule:: gospel.smearing_functions
        :members:

    util
    ----------

    .. automodule:: gospel.util
        :members:

    Density
    ----------

    .. automodule:: gospel.Density
        :members:

    Hamiltonian
    ----------

    .. automodule:: gospel.Hamiltonian
        :members:

    Kpoint
    ----------

    .. automodule:: gospel.Kpoint
        :members:

    Poisson_Solver
    --------------

    .. automodule:: gospel.Poisson_Solver.Poisson_Solver
        :members:

    .. automodule:: gospel.Poisson_Solver.Poisson_ISF
        :members:

    Eigensolver
    -------------

    .. automodule:: gospel.Eigensolver.Eigensolver
        :members:

    .. automodule:: gospel.Eigensolver.CG
        :members:

    .. automodule:: gospel.Eigensolver.Scipy
        :members:

    .. automodule:: gospel.Eigensolver.Tucker
        :members:

    .. automodule:: gospel.Eigensolver.Preconditioner
        :members:

    Pseudopotential
    ----------------

    .. automodule:: gospel.Pseudopotential.Pseudopotential
        :members:

    .. automodule:: gospel.Pseudopotential.ReadUPF
        :members:

    .. automodule:: gospel.Pseudopotential.Local_Potential
        :members:

    .. automodule:: gospel.Pseudopotential.KB_proj
        :members:

    .. automodule:: gospel.Pseudopotential.Compensation
        :members:

    .. automodule:: gospel.Pseudopotential.Supersampling
        :members:

    XC
    ----------

    .. automodule:: gospel.XC.XC_Functional
        :members:

    .. automodule:: gospel.XC.LibXC
        :members:

    .. automodule:: gospel.XC.LDA
        :members:

    .. automodule:: gospel.XC.GGA
        :members:
